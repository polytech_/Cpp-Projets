#ifndef CMatriceH
#define CMatriceH 0

#define DENOMINATEUR_NUL 100
#define ERREUR_PRODUIT_MATR 101
#define ATTRIBUT_MATRICE_INEXISTANT 102
#define MATRICE_DIFFERENTES_DIMENSIONS 103
#define FICHIER_IMCOMPATIBLE 200
#define TYPE_MATRICE_INCOMPATIBLE 201
#define TYPE_NON_ACCEPTABLE 204


#include "CParseur.h"
#include "CComplexe.h"
#include "CExeption.h"
#include <iostream>

using namespace std;



 template <class Type> class CMatrice {
	private :
		unsigned int uiMATnbColonnes;
		unsigned int uiMATnbLignes;
		Type** pTTMATmatrice;
	

	public :


		/**************************************
		**** Constructeur par d�faut
		***************************************
		**** Entr�e: N�ant
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Initialise les param�tre � leurs valeurs par d�faut.
		**** Exception si : N�ant
		**************************************/
		CMatrice<Type>();



		/**************************************
		**** Constructeur de recopie
		***************************************
		**** Entr�e: objet CMatrice MATparam
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : le constructeur de recopie va recopier les attributs de la matrice mise en param�tre
		**** Exception si : N�ant
		**************************************/
		CMatrice<Type>(CMatrice<Type> &MATparam);



		/**************************************
		**** Constructeur de confort
		***************************************
		**** Entr�e: unsigned int : uiNbColonnes, unsigned int : uiNbLignes, Type** : pTTMatrice.
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : le constructeur de confort va attribuer les param�tres aux attributs.
		**** Exception si : N�ant
		**************************************/
		CMatrice<Type>(unsigned int uiNbColonnes,unsigned int uiNbLignes,Type** pTTMatrice);


		/**************************************
		**** Constructeur de confort
		***************************************
		**** Entr�e: const char*: pcFichier
		**** N�cessite : >>>>>>>>Le type doit etre une classe<<<<<<<<
		**** Sortie : N�ant
		**** Entraine : Construction d'une matrice en fonction d'un fichier texte
		**** Exception si : N�ant
		**************************************/
		CMatrice<Type>(const char* pcFichier);



		/**************************************
		**** Constructeur de confort
		***************************************
		**** Entr�e: const char*: pcFichier, bool bMatriceType
		**** N�cessite : >>>>>>>>Le type doit etre un type basique non classe !!<<<<<<<<
		**** Sortie : N�ant
		**** Entraine : Construction d'une matrice en fonction d'un fichier texte
		**** Exception si : N�ant
		**************************************/
		CMatrice<Type>(const char* pcFichier, bool bMatriceType);



		/**************************************
		**** Accesseur en lecture
		***************************************
		**** Entr�e: N�ant
		**** Sortie : N�ant
		**** Entraine : retourne le nombre de colonnes de l'objet matrice en cours
		**** Exception si : N�ant
		**************************************/
		unsigned int MATgetNbColonnes();



		/**************************************
		**** Accesseur en lecture
		***************************************
		**** Entr�e: N�ant
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : retourne le nombre de lignes de l'objet matrice en cours
		**** Exception si : N�ant
		**************************************/
		unsigned int MATgetNbLignes();
		


		/**************************************
		**** Accesseur en lecture
		***************************************
		**** Entr�e: N�ant
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : retourne l'attribut pTTMATmatrice de l'objet matrice en cours
		**** Exception si : pTTMATmatrice est inexistant (=nullptr)
		**************************************/
		Type** getMatrice();
		


		/**************************************
		**** M�thode permettant d'afficher une matrice
		***************************************
		**** Entr�e: N�ant
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Affichage de la matrice dans la console
		**** Exception si : N�ant
		**************************************/
		void MATafficherMatrice();






		/**************************************
		**** Surcharge de l'op�rateur +
		***************************************
		**** Entr�e: MATparam : CMatrice
		**** N�cessite : N�ant
		**** Sortie : CMatrice
		**** Entraine : Additionne l'objet CMatrice en cours avec l'objet CMatrice en param�tre
		**** Exception si : Le nombre de colonnes et de lignes de l'objet en cours
					=/= du nombre de colonnes et de lignes de l'objet en param�tre
		**************************************/
		CMatrice<Type> operator+( CMatrice &MATparam) ;



		/**************************************
		**** Surcharge de l'op�rateur -
		***************************************
		**** Entr�e: MATparam : CMatrice
		**** N�cessite : N�ant
		**** Sortie : CMatrice
		**** Entraine : Soustrait l'objet CMatrice en param�tre de l'objet CMatrice en cours
		**** Exception si : Le nombre de colonnes et de lignes de l'objet en cours
					=/= du nombre de colonnes et de lignes de l'objet en param�tre
		**************************************/
		CMatrice<Type> operator-( CMatrice &MATparam) ;



		/**************************************
		**** Surcharge de l'op�rateur *
		***************************************
		**** Entr�e: MATparam : CMatrice
		**** N�cessite : N�ant
		**** Sortie : CMatrice
		**** Entraine : Multiplie l'objet CMatrice en param�tre par l'objet CMatrice en cours
		**** Exception si : Le nombre de colonnes de l'objet en cours =/= du nombre de lignes de l'objet en param�tre
		**************************************/
		CMatrice<Type> operator*( CMatrice &MATparam) ;



		/**************************************
		**** Surcharge de l'op�rateur *
		***************************************
		**** Entr�e: iParam : entier
		**** N�cessite : N�ant
		**** Sortie : CMatrice
		**** Entraine : Multiplie l'entier pass� en param�tre par l'objet CMatrice en cours
		**** Exception si : N�ant
		**************************************/
		CMatrice<Type> operator*(double iParam);



		/**************************************
		**** Surcharge de l'op�rateur /
		***************************************
		**** Entr�e: iParam : entier
		**** N�cessite : N�ant
		**** Sortie : CMatrice
		**** Entraine : Divise l'objet CMatrice en cours par l'entier pass� en param�tre
		**** Exception si : N�ant
		**************************************/
		CMatrice<Type> operator/(double iParam);



		/**************************************
		**** Surcharge de l'op�rateur =
		***************************************
		**** Entr�e: MATparam : CMatrice
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Affecte l'objet CMatrice pass� en param�tre � l'objet CMatrice en cours
		**** Exception si : N�ant
		**************************************/
		void operator=(CMatrice<Type> const& MATparam);



		/**************************************
		**** Destructeur
		***************************************
		**** Entr�e: N�ant
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : le destructeur va lib�rer le tableau de double dimension de la m�moire 
		**************************************/
		~CMatrice<Type>();



};





#include "CMatrice.cpp"

#endif

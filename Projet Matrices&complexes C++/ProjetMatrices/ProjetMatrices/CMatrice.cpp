#ifndef CMatriceCPP
#define CMatriceCPP 1




#include "CMatrice.h"





template <class Type>  CMatrice<Type>::CMatrice(){
	uiMATnbColonnes = 0;
	uiMATnbLignes = 0;
	pTTMATmatrice = nullptr;
	
}




template <class Type> CMatrice<Type>::~CMatrice() {
	unsigned int uiBoucle;
	for (uiBoucle = 0; uiBoucle < uiMATnbLignes; uiBoucle++) {
		free(pTTMATmatrice[uiBoucle]);
	}
	free(pTTMATmatrice);

}




template <class Type> CMatrice<Type>::CMatrice(CMatrice<Type> &MATparam){
	unsigned int uiBoucleLignes;
	unsigned int uiBoucleColonnes;

	uiMATnbColonnes = MATparam.uiMATnbColonnes;
	uiMATnbLignes = MATparam.uiMATnbLignes;

	pTTMATmatrice = (Type**)malloc(uiMATnbLignes*sizeof(Type*));
	for( uiBoucleLignes=0;uiBoucleLignes<uiMATnbLignes;uiBoucleLignes++){
		pTTMATmatrice[uiBoucleLignes] = (Type*)malloc(uiMATnbColonnes*sizeof(Type));
		for( uiBoucleColonnes=0;uiBoucleColonnes<uiMATnbColonnes;uiBoucleColonnes++)
			pTTMATmatrice[uiBoucleLignes][uiBoucleColonnes] = MATparam.pTTMATmatrice[uiBoucleLignes][uiBoucleColonnes];
	}
	
}




template <class Type>  CMatrice<Type>::CMatrice(unsigned int uiNbColonnes,unsigned int uiNbLignes,Type** pTTMatrice){
	unsigned int uiBoucleLignes;
	unsigned int uiBoucleColonnes;

	uiMATnbColonnes = uiNbColonnes;
	uiMATnbLignes = uiNbLignes;

	
	pTTMATmatrice = (Type**)malloc(uiMATnbLignes*sizeof(Type*));
	for( uiBoucleLignes=0;uiBoucleLignes<uiMATnbLignes;uiBoucleLignes++){
		pTTMATmatrice[uiBoucleLignes] = (Type*)malloc(uiMATnbColonnes*sizeof(Type));
		for( uiBoucleColonnes=0;uiBoucleColonnes<uiMATnbColonnes;uiBoucleColonnes++)
			pTTMATmatrice[uiBoucleLignes][uiBoucleColonnes] =pTTMatrice[uiBoucleLignes][uiBoucleColonnes];
	}

}




template<class Type> CMatrice<Type>::CMatrice(const char * pcFichier)
{
	unsigned int uiBoucle = 0;
	unsigned int uiBoucleParam = 0;
	CParseur* pPARParseur = new CParseur();
	pPARParseur->PARLecture(pcFichier);

	for (uiBoucle = 0; uiBoucle < pPARParseur->PARGetListeElement()->LISGetTaille(); uiBoucle++) {

		switch (uiBoucle)
		{
		case 0:
			//Premi�re ligne : Verification du type
			if (strstr(pPARParseur->PARGetListeElement()->LISGetElem(uiBoucle)->ELPGetNomElem(), "TypeMatrice"))
			{
				if (!strstr(typeid(Type).name(), *pPARParseur->PARGetListeElement()->LISGetElem(uiBoucle)->ELPGetValeurElem()->LISGetElem(0))) {
					CExeption EXC1(TYPE_MATRICE_INCOMPATIBLE);
					throw EXC1;
				}


			}

			else {
				CExeption EXC1(FICHIER_IMCOMPATIBLE);
				throw EXC1;
			}

			break;


			//Deuxi�me ligne : r�cup�ration du nombre de ligne
		case 1:
			if (strstr(pPARParseur->PARGetListeElement()->LISGetElem(uiBoucle)->ELPGetNomElem(), "NBLignes"))
			{
				uiMATnbLignes = atoi(*pPARParseur->PARGetListeElement()->LISGetElem(uiBoucle)->ELPGetValeurElem()->LISGetElem(0));
			}

			else {
				CExeption EXC1(FICHIER_IMCOMPATIBLE);
				throw EXC1;
			}


			break;

			//Troisieme ligne : r�cup�ration du nombre de colonnes
		case 2:
			if (strstr(pPARParseur->PARGetListeElement()->LISGetElem(uiBoucle)->ELPGetNomElem(), "NBColonnes"))
			{
				uiMATnbColonnes = atoi(*pPARParseur->PARGetListeElement()->LISGetElem(uiBoucle)->ELPGetValeurElem()->LISGetElem(0));
			}

			else {
				CExeption EXC1(FICHIER_IMCOMPATIBLE);
				throw EXC1;
			}


			break;

			//Quatrieme ligne : r�cup�ration de la matrice
		case 3:

			if (strstr(pPARParseur->PARGetListeElement()->LISGetElem(uiBoucle)->ELPGetNomElem(), "Matrice"))
			{
				int iCompteur = 0;
				int	iSousCompteur = 0;
				unsigned int uiBoucleLigne = 0;
				unsigned int uiBoucleColonnes = 0;
				unsigned int uiBoucleChar = 0;
				char * chaine;
				char sousChaine[] = "              ";
				pTTMATmatrice = (Type**)malloc(uiMATnbLignes * sizeof(Type*));
				for (uiBoucleLigne = 0; uiBoucleLigne < uiMATnbLignes; uiBoucleLigne++)
				{
					chaine = *pPARParseur->PARGetListeElement()->LISGetElem(uiBoucle)->ELPGetValeurElem()->LISGetElem(uiBoucleLigne);
					for (uiBoucleChar = 0; uiBoucleChar < 15; uiBoucleChar++)sousChaine[uiBoucleChar] = ' ';
					sousChaine[14] = '\0';
					pTTMATmatrice[uiBoucleLigne] = (Type*)malloc(uiMATnbColonnes * sizeof(Type));
					while (chaine[iCompteur] != '\0') {
						if (chaine[iCompteur] == ' ') {

							pTTMATmatrice[uiBoucleLigne][uiBoucleColonnes] = Type(sousChaine);

							uiBoucleColonnes++;
							iSousCompteur = 0;
							for (uiBoucleChar = 0; uiBoucleChar < 15; uiBoucleChar++)sousChaine[uiBoucleChar] = ' ';
							sousChaine[14] = '\0';
						}
						else {
							sousChaine[iSousCompteur] = chaine[iCompteur];
							iSousCompteur++;
						}

						iCompteur++;
					}

						pTTMATmatrice[uiBoucleLigne][uiBoucleColonnes] =  Type(sousChaine);
				
					uiBoucleColonnes = 0;
					iCompteur = 0;

				}


			}

			else {
				CExeption EXC1(FICHIER_IMCOMPATIBLE);
				throw EXC1;
			}

			break;

		}


	}
	delete pPARParseur;


}

template<class Type> CMatrice<Type>::CMatrice(const char * pcFichier, bool bMatriceType)
{
	unsigned int uiBoucle = 0;
	unsigned int uiBoucleParam = 0;
	CParseur* pPARParseur = new CParseur();
	pPARParseur->PARLecture(pcFichier);

	for (uiBoucle = 0; uiBoucle < pPARParseur->PARGetListeElement()->LISGetTaille(); uiBoucle++) {

		switch (uiBoucle)
		{
		case 0:
			//Premi�re ligne : Verification du type
			if (strstr(pPARParseur->PARGetListeElement()->LISGetElem(uiBoucle)->ELPGetNomElem(), "TypeMatrice"))
			{
				if (!strstr(typeid(Type).name(), *pPARParseur->PARGetListeElement()->LISGetElem(uiBoucle)->ELPGetValeurElem()->LISGetElem(0))) {
					if (typeid(Type) == typeid(double) || typeid(Type) == typeid(int)) {
						CExeption EXC1(TYPE_NON_ACCEPTABLE);
						throw EXC1;
					}
					else {
						CExeption EXC1(TYPE_MATRICE_INCOMPATIBLE);
						throw EXC1;
					}

				}


			}

			else {
				CExeption EXC1(FICHIER_IMCOMPATIBLE);
				throw EXC1;
			}

			break;


			//Deuxi�me ligne : r�cup�ration du nombre de ligne
		case 1:
			if (strstr(pPARParseur->PARGetListeElement()->LISGetElem(uiBoucle)->ELPGetNomElem(), "NBLignes"))
			{
				uiMATnbLignes = atoi(*pPARParseur->PARGetListeElement()->LISGetElem(uiBoucle)->ELPGetValeurElem()->LISGetElem(0));
			}

			else {
				CExeption EXC1(FICHIER_IMCOMPATIBLE);
				throw EXC1;
			}


			break;

			//Troisieme ligne : r�cup�ration du nombre de colonnes
		case 2:
			if (strstr(pPARParseur->PARGetListeElement()->LISGetElem(uiBoucle)->ELPGetNomElem(), "NBColonnes"))
			{
				uiMATnbColonnes = atoi(*pPARParseur->PARGetListeElement()->LISGetElem(uiBoucle)->ELPGetValeurElem()->LISGetElem(0));
			}

			else {
				CExeption EXC1(FICHIER_IMCOMPATIBLE);
				throw EXC1;
			}


			break;

			//Quatrieme ligne : r�cup�ration de la matrice
		case 3:

			if (strstr(pPARParseur->PARGetListeElement()->LISGetElem(uiBoucle)->ELPGetNomElem(), "Matrice"))
			{
				int iCompteur = 0;
				int	iSousCompteur = 0;
				unsigned int uiBoucleLigne = 0;
				unsigned int uiBoucleColonnes = 0;
				unsigned int uiBoucleChar = 0;
				char * chaine;
				char sousChaine[] = "              ";
				pTTMATmatrice = (Type**)malloc(uiMATnbLignes * sizeof(Type*));
				for (uiBoucleLigne = 0; uiBoucleLigne < uiMATnbLignes; uiBoucleLigne++)
				{
					chaine = *pPARParseur->PARGetListeElement()->LISGetElem(uiBoucle)->ELPGetValeurElem()->LISGetElem(uiBoucleLigne);
					for (uiBoucleChar = 0; uiBoucleChar < 15; uiBoucleChar++)sousChaine[uiBoucleChar] = ' ';
					sousChaine[14] = '\0';
					pTTMATmatrice[uiBoucleLigne] = (Type*)malloc(uiMATnbColonnes * sizeof(Type));
					while (chaine[iCompteur] != '\0') {
						if (chaine[iCompteur] == ' ') {

							if (typeid(Type) == typeid(double)) {
								pTTMATmatrice[uiBoucleLigne][uiBoucleColonnes] = atof(sousChaine);

							}
							else if (typeid(Type) == typeid(int)) {
								pTTMATmatrice[uiBoucleLigne][uiBoucleColonnes] = atoi(sousChaine);

							}
							else {
								CExeption EXC1(TYPE_NON_ACCEPTABLE);
								throw EXC1;
							}
							uiBoucleColonnes++;
							iSousCompteur = 0;
							for (uiBoucleChar = 0; uiBoucleChar < 15; uiBoucleChar++)sousChaine[uiBoucleChar] = ' ';
							sousChaine[14] = '\0';
						}
						else {
							sousChaine[iSousCompteur] = chaine[iCompteur];
							iSousCompteur++;
						}

						iCompteur++;
					}

					if (typeid(Type) == typeid(double)) {
						pTTMATmatrice[uiBoucleLigne][uiBoucleColonnes] = atof(sousChaine);

					}
					else if (typeid(Type) == typeid(int)) {
						pTTMATmatrice[uiBoucleLigne][uiBoucleColonnes] = atoi(sousChaine);

					}
					else {
						CExeption EXC1(TYPE_NON_ACCEPTABLE);
						throw EXC1;
					}
					uiBoucleColonnes = 0;
					iCompteur = 0;

				}


			}

			else {
				CExeption EXC1(FICHIER_IMCOMPATIBLE);
				throw EXC1;
			}

			break;

		}


	}
	delete pPARParseur;

}






template <class Type> Type** CMatrice<Type>::getMatrice() {

	if (pTTMATmatrice==nullptr)
	{
		CExeption EXCprdMat(ATTRIBUT_MATRICE_INEXISTANT);
		throw EXCprdMat;
	}
	
	return pTTMATmatrice;
}




template <class Type> unsigned int CMatrice<Type>::MATgetNbColonnes(){
	return uiMATnbColonnes;
}




template <class Type> unsigned int CMatrice<Type>::MATgetNbLignes(){
	return uiMATnbLignes;
}




template <class Type> void CMatrice<Type>::MATafficherMatrice(){

	unsigned int uiBoucleLignes;
	unsigned int uiBoucleColonnes;

	for( uiBoucleLignes=0;uiBoucleLignes<uiMATnbLignes;uiBoucleLignes++){
		for (uiBoucleColonnes = 0; uiBoucleColonnes < uiMATnbColonnes; uiBoucleColonnes++) {
			cout << pTTMATmatrice[uiBoucleLignes][uiBoucleColonnes] << "\t";
		}
			
		
		cout << "\n";
	}

	cout << "\n";
	
}








template <class Type> CMatrice<Type> CMatrice<Type>:: operator*(double iParam) {

	unsigned int uiBoucleLignes;
	unsigned int uiBoucleColonnes;

	Type** pTTResultat = nullptr;

	pTTResultat = (Type**)malloc(uiMATnbLignes*sizeof(Type*));
	for( uiBoucleLignes=0;uiBoucleLignes<uiMATnbLignes;uiBoucleLignes++){
		pTTResultat[uiBoucleLignes] = (Type*)malloc(uiMATnbColonnes*sizeof(Type));
		for( uiBoucleColonnes=0;uiBoucleColonnes<uiMATnbColonnes;uiBoucleColonnes++)
			pTTResultat[uiBoucleLignes][uiBoucleColonnes]=pTTMATmatrice[uiBoucleLignes][uiBoucleColonnes]*iParam;
	}

	CMatrice<Type> MATRes(uiBoucleColonnes,uiBoucleLignes,pTTResultat);
	return MATRes;

}




template <class Type> CMatrice<Type> CMatrice<Type>:: operator/(double iParam){

	unsigned int uiBoucleLignes;
	unsigned int uiBoucleColonnes;

	Type** pTTResultat = nullptr;

	pTTResultat = (Type**)malloc(uiMATnbLignes*sizeof(Type*));
	for( uiBoucleLignes=0;uiBoucleLignes<uiMATnbLignes;uiBoucleLignes++){
		pTTResultat[uiBoucleLignes] = (Type*)malloc(uiMATnbColonnes*sizeof(Type));
		for( uiBoucleColonnes=0;uiBoucleColonnes<uiMATnbColonnes;uiBoucleColonnes++)
			pTTResultat[uiBoucleLignes][uiBoucleColonnes] =pTTMATmatrice[uiBoucleLignes][uiBoucleColonnes]/iParam;
	}
	CMatrice<Type> MATRes(uiBoucleColonnes,uiBoucleLignes,pTTResultat);
	return MATRes;






}




template <class Type> CMatrice<Type> CMatrice<Type>::operator+( CMatrice &MATparam)  {
	
	
	
	unsigned int uiBoucleLignes;
	unsigned int uiBoucleColonnes;

	if (MATparam.MATgetNbLignes() == 0 && MATparam.MATgetNbColonnes() == 0) {
		return *this;
	}
	if (this->MATgetNbLignes() == 0 && this->MATgetNbColonnes() == 0) {
		return MATparam;
	}

	//Si les matrices ne sont pas de m�me dimension i.e : n'ont pas le m�me nombre de lignes et de colonnes
	if ((uiMATnbColonnes != MATparam.MATgetNbColonnes()) || (uiMATnbLignes != MATparam.MATgetNbLignes()))
	{
		CExeption EXCprdMat(MATRICE_DIFFERENTES_DIMENSIONS);
		throw EXCprdMat;
	}



	Type** pTTResultat = nullptr;

	pTTResultat = (Type**)malloc(uiMATnbLignes * sizeof(Type*));
	for (uiBoucleLignes = 0; uiBoucleLignes < uiMATnbLignes; uiBoucleLignes++) {
		pTTResultat[uiBoucleLignes] = (Type*)malloc(uiMATnbColonnes * sizeof(Type));
		for (uiBoucleColonnes = 0; uiBoucleColonnes < uiMATnbColonnes; uiBoucleColonnes++)
			pTTResultat[uiBoucleLignes][uiBoucleColonnes] = pTTMATmatrice[uiBoucleLignes][uiBoucleColonnes] + MATparam.pTTMATmatrice[uiBoucleLignes][uiBoucleColonnes];
	}
	CMatrice<Type> MATRes(uiBoucleColonnes, uiBoucleLignes, pTTResultat);
	return MATRes;


}




template <class Type> CMatrice<Type> CMatrice<Type>::operator-( CMatrice &MATparam)  {

	

	unsigned int uiBoucleLignes;
	unsigned int uiBoucleColonnes;


	if (MATparam.MATgetNbLignes() == 0 && MATparam.MATgetNbColonnes() == 0) {
		return *this;
	}
	if (this->MATgetNbLignes() == 0 && this->MATgetNbColonnes() == 0) {
		return MATparam;
	}

	//Si les matrices ne sont pas de m�me dimension i.e : n'ont pas le m�me nombre de lignes et de colonnes
	if (uiMATnbColonnes != MATparam.MATgetNbColonnes() && uiMATnbLignes != MATparam.MATgetNbLignes())
	{
		CExeption EXCprdMat(MATRICE_DIFFERENTES_DIMENSIONS);
		throw EXCprdMat;
	}

	Type** pTTResultat = nullptr;

	pTTResultat = (Type**)malloc(uiMATnbLignes * sizeof(Type*));
	for (uiBoucleLignes = 0; uiBoucleLignes < uiMATnbLignes; uiBoucleLignes++) {
		pTTResultat[uiBoucleLignes] = (Type*)malloc(uiMATnbColonnes * sizeof(Type));
		for (uiBoucleColonnes = 0; uiBoucleColonnes < uiMATnbColonnes; uiBoucleColonnes++)
			pTTResultat[uiBoucleLignes][uiBoucleColonnes] = pTTMATmatrice[uiBoucleLignes][uiBoucleColonnes] - MATparam.pTTMATmatrice[uiBoucleLignes][uiBoucleColonnes];
	}
	CMatrice<Type> MATRes(uiBoucleColonnes, uiBoucleLignes, pTTResultat);
	return MATRes;


}




template <class Type> CMatrice<Type> CMatrice<Type>::operator*(CMatrice<Type> &MATparam)  {


	if (MATparam.MATgetNbLignes() == 0 && MATparam.MATgetNbColonnes() == 0) {
		return *this;
	}
	if (this->MATgetNbLignes() == 0 && this->MATgetNbColonnes() == 0) {
		return MATparam;
	}


	if (uiMATnbColonnes != MATparam.MATgetNbLignes())
	{
		CExeption EXCprdMat(ERREUR_PRODUIT_MATR);
		throw EXCprdMat;
	}

		
		

	unsigned int uiBoucle;
	unsigned int uiBoucleLignes;
	unsigned int uiBoucleColonnes;

	Type** pTTResultat = nullptr;

	//Allocation
	pTTResultat = (Type**)malloc(uiMATnbLignes * sizeof(Type*));
	for (uiBoucleLignes = 0; uiBoucleLignes < uiMATnbLignes; uiBoucleLignes++) {
		pTTResultat[uiBoucleLignes] = (Type*)malloc(MATparam.MATgetNbColonnes() * sizeof(Type));
	}

	//Remplissage


	for (uiBoucleLignes = 0; uiBoucleLignes < uiMATnbLignes; uiBoucleLignes++) {
		for (uiBoucleColonnes = 0; uiBoucleColonnes < MATparam.MATgetNbColonnes(); uiBoucleColonnes++)
		{
			pTTResultat[uiBoucleLignes][uiBoucleColonnes] = 0;

			for (uiBoucle = 0; uiBoucle < uiMATnbColonnes; uiBoucle++)
			{
				pTTResultat[uiBoucleLignes][uiBoucleColonnes] += pTTMATmatrice[uiBoucleLignes][uiBoucle] * MATparam.pTTMATmatrice[uiBoucle][uiBoucleColonnes];

			}
		}
			
	}
	CMatrice<Type> MATRes(uiBoucleColonnes, uiBoucleLignes, pTTResultat);
	return MATRes;

}




template <class Type> void CMatrice<Type>::operator=(CMatrice<Type> const& MATparam) {



	unsigned int uiBoucleLignes;
	unsigned int uiBoucleColonnes;



	if (uiMATnbLignes > 0 && uiMATnbColonnes > 0) {
		for (uiBoucleLignes = 0; uiBoucleLignes < uiMATnbLignes; uiBoucleLignes++) {
			free(pTTMATmatrice[uiBoucleLignes]);
		}
		free(pTTMATmatrice);
	}

	uiMATnbColonnes = MATparam.uiMATnbColonnes;
	uiMATnbLignes = MATparam.uiMATnbLignes;

	pTTMATmatrice = (Type**)malloc(uiMATnbLignes * sizeof(Type*));
	for (uiBoucleLignes = 0; uiBoucleLignes < uiMATnbLignes; uiBoucleLignes++) {
		pTTMATmatrice[uiBoucleLignes] = (Type*)malloc(uiMATnbColonnes * sizeof(Type));
		for (uiBoucleColonnes = 0; uiBoucleColonnes < uiMATnbColonnes; uiBoucleColonnes++)
			pTTMATmatrice[uiBoucleLignes][uiBoucleColonnes] = MATparam.pTTMATmatrice[uiBoucleLignes][uiBoucleColonnes];
	}



}




template<class Type> ostream &operator<<(ostream &flux, CMatrice<Type> &MATMatrice)
{
	unsigned int iBoucle;
	unsigned int jBoucle;

	for (iBoucle=0; iBoucle < MATMatrice.MATgetNbLignes(); iBoucle++) {
		for (jBoucle=0; jBoucle < MATMatrice.MATgetNbColonnes(); jBoucle++) {
			flux << MATMatrice.getMatrice()[iBoucle][jBoucle] << "\t";
		}
		flux << "\n";
	}

	return flux;
}




template <class Type> CMatrice<Type> operator*(int iparam, CMatrice<Type> & MATparam) {
	unsigned int uiBoucleLignes;
	unsigned int uiBoucleColonnes;

	Type** pTTResultat = nullptr;

	pTTResultat = (Type**)malloc(MATparam.MATgetNbLignes() * sizeof(Type*));
	for (uiBoucleLignes = 0; uiBoucleLignes < MATparam.MATgetNbLignes(); uiBoucleLignes++) {
		pTTResultat[uiBoucleLignes] = (Type*)malloc(MATparam.MATgetNbColonnes() * sizeof(Type));
		for (uiBoucleColonnes = 0; uiBoucleColonnes < MATparam.MATgetNbColonnes(); uiBoucleColonnes++)
			pTTResultat[uiBoucleLignes][uiBoucleColonnes] = MATparam.getMatrice()[uiBoucleLignes][uiBoucleColonnes] * iparam;
	}

	CMatrice<Type> MATRes(uiBoucleColonnes, uiBoucleLignes, pTTResultat);
	return MATRes;
}





#endif
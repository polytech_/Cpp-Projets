// ProjetMatrices.cpp�: d�finit le point d'entr�e pour l'application console.

#include "CMatrice.h"
#include "COperationMatComplexes.h"
#include "COperationMat.h"
#include <iostream>
//#include "vld.h"

using namespace std;



int main(unsigned int argc, char *argv[])
{


	CMatrice<CComplexe> ** pMATMATtabMatrice;
	unsigned int uiBoucle;
	double dValeur;

	pMATMATtabMatrice = (CMatrice<CComplexe>**)malloc(argc * sizeof(CMatrice<CComplexe>*));
	for (uiBoucle = 1; uiBoucle < argc; uiBoucle++) {
		try {
			pMATMATtabMatrice[uiBoucle - 1] = new CMatrice<CComplexe>(argv[uiBoucle]);

		}
		catch (CExeption EXCprdMat) {
			if(EXCprdMat.EXCLireNErreur() == TYPE_NON_ACCEPTABLE)
				cerr << "Vous avez construit une matrice de type basique, votre type est incompatible ou non implemente, code numero : " << EXCprdMat.EXCLireNErreur() << endl;

			else
				cerr << "Type different de CComplexe ou Fichier introuvable/incompatible avec le modele, merci de le modifier/le mettre en parametre, code numero :" << EXCprdMat.EXCLireNErreur() << endl;
			return EXIT_FAILURE;
		}

	}


	
	cout << "Veuillez rentrer une valeur :" << endl;
	cin >> dValeur;



	cout << "\n\n\nMULTIPLICATION DES MATRICES PAR " << dValeur << " :\n" << endl;
	for (uiBoucle = 0; uiBoucle < argc - 1; uiBoucle++) {
		cout << "Matrice" << uiBoucle + 1 << " * " << dValeur << "=" << endl;
		cout << *pMATMATtabMatrice[uiBoucle] * dValeur << endl;
	}


	cout << "\n\n\nDIVISION DES MATRICES PAR " << dValeur << " :\n" << endl;
	for (uiBoucle = 0; uiBoucle < argc - 1; uiBoucle++) {
		try {
			cout << "Matrice" << uiBoucle + 1 << " / " << dValeur << "=" << endl;
			cout << *pMATMATtabMatrice[uiBoucle] / dValeur << endl;
		}
		catch (CExeption EXCprdMat) { cerr << "Division par 0 impossible, code numero :" << EXCprdMat.EXCLireNErreur() << endl; }
		
	}





	CMatrice<CComplexe> *pMATResultatSomme = new CMatrice<CComplexe>();

	cout << "\n\n\nSOMME DES MATRICES ENTRE ELLES" << " :\n" << endl;
	for (uiBoucle = 0; uiBoucle < argc - 1; uiBoucle++) {
		if (uiBoucle != argc - 2)
			cout << "Matrice" << uiBoucle + 1 << " + ";
		else
			cout << "Matrice" << uiBoucle + 1 << " = \n" << endl;
	}

	for (uiBoucle = 0; uiBoucle < argc - 1; uiBoucle++) {
		try {
			*pMATResultatSomme = *pMATResultatSomme + *pMATMATtabMatrice[uiBoucle];
		}
		catch (CExeption EXCprdMat) { cerr << "Somme impossible, code numero :" << EXCprdMat.EXCLireNErreur() << endl; }

	}

	cout << *pMATResultatSomme << endl;



	CMatrice<CComplexe> *pMATResultatSommeAltern = new CMatrice<CComplexe>();


	cout << "\n\n\nSOMME ALTERNEE DES MATRICES" << " :\n" << endl;
	for (uiBoucle = 0; uiBoucle < argc - 1; uiBoucle++) {
		if (uiBoucle == argc - 2)
			cout << "Matrice" << uiBoucle + 1 << " = \n" << endl;
		else if (uiBoucle % 2 == 0)
			cout << "Matrice" << uiBoucle + 1 << " - ";
		else if (uiBoucle % 2 == 1)
			cout << "Matrice" << uiBoucle + 1 << " + ";

	}

	for (uiBoucle = 0; uiBoucle < argc - 1; uiBoucle++) {
		if (uiBoucle % 2 == 1) {
			try {
				*pMATResultatSommeAltern = *pMATResultatSommeAltern - *pMATMATtabMatrice[uiBoucle];
			}
			catch (CExeption EXCprdMat) { cerr << "Soustraction impossible, code numero :" << EXCprdMat.EXCLireNErreur() << endl; }

		}


		if (uiBoucle % 2 == 0) {
			try {
				*pMATResultatSommeAltern = *pMATResultatSommeAltern + *pMATMATtabMatrice[uiBoucle];
			}
			catch (CExeption EXCprdMat) { cerr << "Somme impossible, code numero :" << EXCprdMat.EXCLireNErreur() << endl; }
		}



	}

	cout << *pMATResultatSommeAltern << endl;



	CMatrice<CComplexe> *pMATResultatProduit = new CMatrice<CComplexe>();


	cout << "\n\n\nPRODUIT DES MATRICES ENTRE ELLES" << " :\n" << endl;
	for (uiBoucle = 0; uiBoucle < argc - 1; uiBoucle++) {
		if (uiBoucle != argc - 2)
			cout << "Matrice" << uiBoucle + 1 << " * ";
		else
			cout << "Matrice" << uiBoucle + 1 << " = \n" << endl;
	}

	for (uiBoucle = 0; uiBoucle < argc - 1; uiBoucle++) {
		try {
			*pMATResultatProduit = *pMATResultatProduit * *pMATMATtabMatrice[uiBoucle];
		}
		catch (CExeption EXCprdMat) { cerr << "Produit impossible, code numero :" << EXCprdMat.EXCLireNErreur() << endl; }

	}
	cout << *pMATResultatProduit << endl;


	
	cout << "\n\n\PARTIES REELLES DES MATRICES  :\n" << endl;
	for (uiBoucle = 0; uiBoucle < argc - 1; uiBoucle++) {
		cout << "Matrice" << uiBoucle + 1 << "=" << endl;
		cout << COperationMatComplexes::OMCGetPartieReelle(*pMATMATtabMatrice[uiBoucle]) << endl;
	}

	cout << "\n\n\PARTIES IMAGINAIRES DES MATRICES  :\n" << endl;
	for (uiBoucle = 0; uiBoucle < argc - 1; uiBoucle++) {
		cout << "Matrice" << uiBoucle + 1 << "=" << endl;
		cout << COperationMatComplexes::OMCGetPartieImaginaire(*pMATMATtabMatrice[uiBoucle]) << endl;
	}

	cout << "\n\n\TRANSPOSEES DES MATRICES  :\n" << endl;
	for (uiBoucle = 0; uiBoucle < argc - 1; uiBoucle++) {
		cout << "Matrice" << uiBoucle + 1 << "=" << endl;
		cout << COperationMat::OPMTransposee(*pMATMATtabMatrice[uiBoucle]) << endl;
	}
	
	//Lib�ration de la m�moire

	delete pMATResultatSomme;

	delete pMATResultatSommeAltern;

	delete pMATResultatProduit;


	for (uiBoucle = 1; uiBoucle < argc; uiBoucle++) {
		free(pMATMATtabMatrice[uiBoucle - 1]);
	}
	free(pMATMATtabMatrice);
}







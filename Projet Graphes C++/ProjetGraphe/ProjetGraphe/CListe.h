#pragma once
#ifndef CListe
#define CListeH 4
#include <stdlib.h>
#include <iostream>
#include <stdio.h>

using namespace std;

template<class Type> class CListe
{
	private:
		Type ** pTTLISListe;
		unsigned int uiLISTaille;


	public:


		/**************************************
		**** Constructeur par d�faut
		***************************************
		**** Entr�e: N�ant
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Initilise l'attribut pTTLISListe � nullptr et uiLISTaille � 0
		**** Exception si : N�ant
		**************************************/
		CListe();


		/**************************************
		**** Destucteur
		***************************************
		**** Entr�e: N�ant
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Lib�re l'espace m�moire occup� par pTTLISListe
		**** Exception si : N�ant
		**************************************/
		~CListe();


		/**************************************
		**** M�thode d'ajout d'un �l�ment
		***************************************
		**** Entr�e: pTElem : pointeur sur l'�lement que l'on veut ajouter
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Ajoute l'�lement point� par pTElem dans la liste pTTLISListe
		**** Exception si : N�ant
		**************************************/
		void LISAJouter(Type *pTElem);


		/**************************************
		**** M�thode de suppression d'un �l�ment
		***************************************
		**** Entr�e: pTElem : pointeur sur l'�lement que l'on veut ajouter
		**** N�cessite : N�ant
		**** Sortie : bool
		**** Entraine : Supprime l'�lement point� par pTElem dans la liste pTTLISListe
		**** Exception si : N�ant
		**************************************/
		bool LISSupprimerElem(Type *pTElem);



		/**************************************
		**** Surcharge de l'op�rateur []
		***************************************
		**** Entr�e: uiPos : Position de l'�lement
		**** N�cessite : N�ant
		**** Sortie : Type*
		**** Entraine : Retourne un pointeur sur l'�lement se trouvant � la position 
									uiPos dans pTTLISListe
		**** Exception si : N�ant
		**************************************/
		Type* operator[](unsigned int uiPos);



		/**************************************
		**** Getter de la taille de la liste
		***************************************
		**** Entr�e: N�ant
		**** N�cessite : N�ant
		**** Sortie : unsigned int
		**** Entraine : Retourne l'attribut uiLISTaille
		**** Exception si : N�ant
		**************************************/
		unsigned int LISGetTaille();


		/**************************************
		**** Getter d'un �lement de la liste
		***************************************
		**** Entr�e: uiPos : position de l'�lement dans la liste pTTLISListe
		**** N�cessite : N�ant
		**** Sortie : Type*
		**** Entraine : Retourne un pointeur sur l'�lement pr�sent � la position uiPos dans pTTLISListe
		**** Exception si : N�ant
		**************************************/
		Type* LISGetElem(unsigned int uiPos);


		/**************************************
		**** Getter de la liste
		***************************************
		**** Entr�e: N�ant
		**** N�cessite : N�ant
		**** Sortie : Type**
		**** Entraine : Retourne l'attribut pTTLISListe
		**** Exception si : N�ant
		**************************************/
		Type** LISgetListe();


};

#include "CListe.cpp"





#endif // !CListe




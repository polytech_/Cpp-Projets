#pragma once
#ifndef CElementParseurH
#define CElementParseurH 7
#include"CListe.h"



class CElementParseur
{

private:

	char* pcELPNomElement;


	CListe<char*>* pLISELPValeurElement;


public:

	/////Tu copies ce mod�le de commentaires pour les autres m�thodes ://////

	/**************************************
	**** Constructeur par d�faut
	***************************************
	**** Entr�e: N�ant
	**** N�cessite : N�ant
	**** Sortie : N�ant
	**** Entraine : N�ant
	**** Exception si : N�ant
	**************************************/
	CElementParseur();

	/**************************************
	**** Constructeur 
	**************************************
	**** Entr�e: -pcNom : chaine de caractere
	****         - pLISElem : pointeur vers une liste de chaine de caractere
	**** N�cessite : N�ant
	**** Sortie : N�ant
	**** Entraine : N�ant
	**** Exception si : N�ant
	**************************************/
	CElementParseur(char* pcNom, CListe<char*>* pLISElem);

	/**************************************
	**** Destructeur
	**************************************
	**** Entr�e: 
	**** N�cessite : N�ant
	**** Sortie : N�ant
	**** Entraine : Destruction de la liste 
	**** Exception si : N�ant
	**************************************/
	~CElementParseur();

	/**************************************
	**** Methode getter 
	**************************************
	**** Entr�e: N�ant
	**** N�cessite : N�ant
	**** Sortie : chaine de caractere
	**** Entraine : retourne la chaine
	**** Exception si : N�ant
	**************************************/
	char* ELPGetNomElem();

	/**************************************
	**** Methode getter
	**************************************
	**** Entr�e: N�ant
	**** N�cessite : N�ant
	**** Sortie : Une liste de chaines
	**** Entraine : retourne la liste en attribut
	**** Exception si : N�ant
	**************************************/
	CListe<char*>* ELPGetValeurElem();

	/**************************************
	**** Methode setter 
	**************************************
	**** Entr�e: -pcNom : chaine de caractere
	**** N�cessite : N�ant
	**** Sortie : N�ant
	**** Entraine : Modification de l'attribut nom
	**** Exception si : N�ant
	**************************************/
	void ELPSetNomElem(char* pcNom);

	/**************************************
	**** Methode getter
	**************************************
	**** Entr�e: -pLISVal : liste de chaine de caractere
	**** N�cessite : N�ant
	**** Sortie : N�ant
	**** Entraine : Modification de l'attribut liste
	**** Exception si : N�ant
	**************************************/
	void ELPSetValeurElem(CListe<char*>* pLISVal);


};




#endif // !CElementParseur 7



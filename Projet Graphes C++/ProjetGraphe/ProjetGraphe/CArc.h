#pragma once
#ifndef CArc
#define CArcH 2




class CArc
{
	private:
		int iDestination;
	public:


		/**************************************
		**** Constructeur de confort
		***************************************
		**** Entr�e: iDest : entier
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Instanciation d'un objet CArc dont la destination est iDest
		**** Exception si : N�ant
		**************************************/
		CArc(int iDest);



		/**************************************
		**** Setter de destination
		***************************************
		**** Entr�e: iDest : entier
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Modifie l'attribut iDestination de l'objet en cours 
				et le remplace par iDest
		**** Exception si : N�ant
		**************************************/
		void ARCSetDestination(int iDest);



		/**************************************
		**** Getter de destination
		***************************************
		**** Entr�e: N�ant
		**** N�cessite : N�ant
		**** Sortie : la destination de l'objet CArc en cours
		**** Entraine : Retourne la destination de l'objet CArc en cours
		**** Exception si : N�ant
		**************************************/
		int ARCGetDestination();
};


#endif // !CArc
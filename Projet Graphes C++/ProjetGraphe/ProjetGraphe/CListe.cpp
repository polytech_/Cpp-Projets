#include "CListe.h"



template<class Type> CListe<Type>::CListe()
{
	uiLISTaille = 0;
	pTTLISListe = nullptr;
}






template<class Type> CListe<Type>::~CListe() {
	unsigned int uiBoucle;
	for (uiBoucle = 0; uiBoucle < uiLISTaille; uiBoucle++) {
		delete pTTLISListe[uiBoucle];
	}
	//pTTLISListe = (Type**)realloc(pTTLISListe, 0);
	//free(pTTLISListe);
}





template<class Type> void CListe<Type>::LISAJouter(Type *pTElem) {

	unsigned int uiBoucle;


	if (uiLISTaille == 0) {
		 pTTLISListe = (Type**)malloc(sizeof(Type*) * 1); 
	}
	else {
		Type ** pTTTempListe = (Type**)malloc(sizeof(Type*)*uiLISTaille);
		

		//(Type**)memcpy(pTTTempListe, pTTLISListe, uiLISTaille);

		
		for (uiBoucle = 0; uiBoucle < uiLISTaille; uiBoucle++) {
			pTTTempListe[uiBoucle] = pTTLISListe[uiBoucle];
		}
		
		
		pTTLISListe = (Type**)realloc(pTTLISListe, sizeof(Type*) * uiLISTaille+1);
		//memcpy(pTTLISListe, pTTTempListe, uiLISTaille);

		for (uiBoucle = 0; uiBoucle < uiLISTaille; uiBoucle++) {
			pTTLISListe[uiBoucle] = pTTTempListe[uiBoucle];
		}
		free(pTTTempListe);
	}

	pTTLISListe[uiLISTaille] = pTElem;
	uiLISTaille++;

}






template<class Type> bool CListe<Type>::LISSupprimerElem(Type *pTElem) {

	unsigned int uiBoucle;
	bool bASupprimmer = false;
	Type ** pTTTempListe = (Type**)malloc(sizeof(Type*)*uiLISTaille-1 );
	for (uiBoucle = 0; uiBoucle < uiLISTaille; uiBoucle++) {

		if (bASupprimmer == true) {
			pTTLISListe[uiBoucle - 1] = pTTLISListe[uiBoucle];
		}
		if (pTTLISListe[uiBoucle] == pTElem) {
			delete pTElem;
			bASupprimmer = true;
		}

	}

	//Reallocation

	for (uiBoucle = 0; uiBoucle < uiLISTaille-1; uiBoucle++) {
		pTTTempListe[uiBoucle] = pTTLISListe[uiBoucle];
	}
	pTTLISListe = (Type**)realloc(pTTLISListe, sizeof(Type*) * uiLISTaille-1);
	for (uiBoucle = 0; uiBoucle < uiLISTaille-1; uiBoucle++) {
		pTTLISListe[uiBoucle] = pTTTempListe[uiBoucle];
	}
	free(pTTTempListe);
	uiLISTaille--;


	return bASupprimmer;

}




template<class Type> Type* CListe<Type>::operator[](unsigned int uiPos) {
	return pTTLISListe[uiPos];

}

template<class Type> unsigned int CListe<Type>::LISGetTaille() {
	return uiLISTaille;
}

template<class Type> Type * CListe<Type>::LISGetElem(unsigned int uiPos)
{
	return pTTLISListe[uiPos];
}


template<class Type> Type ** CListe<Type>::LISgetListe()
{
	return pTTLISListe;
}


template<class Type> ostream &operator<<(ostream &out,  CListe<Type> &liste) {
	unsigned int uiBoucle;
	for (uiBoucle = 0; uiBoucle < liste.LISGetTaille(); uiBoucle++) {
		out << *(liste.LISGetElem(uiBoucle))<<" ";
	}

	return out;
}
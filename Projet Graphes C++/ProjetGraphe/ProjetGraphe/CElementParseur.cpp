#include "CElementParseur.h"



CElementParseur::CElementParseur()
{
	pLISELPValeurElement = nullptr;
	pcELPNomElement = nullptr;
}


CElementParseur::CElementParseur(char * pcNom, CListe<char*>* pLISElem)
{
	pcELPNomElement = pcNom;
	pLISELPValeurElement = pLISElem;
}


CElementParseur::~CElementParseur()
{
	free(pcELPNomElement);
	delete pLISELPValeurElement;

}


char * CElementParseur::ELPGetNomElem()
{
	return pcELPNomElement;
}


CListe<char*>* CElementParseur::ELPGetValeurElem()
{
	return pLISELPValeurElement;
}



void CElementParseur::ELPSetNomElem(char * pcNom)
{
	pcELPNomElement = pcNom;
}



void CElementParseur::ELPSetValeurElem(CListe<char*>*pLISVal)
{
	pLISELPValeurElement = pLISVal;
}


 ostream &operator<<(ostream &out, CElementParseur &elem) {
	 out << "Nom Elem : " << elem.ELPGetNomElem() << endl;
	 out << "Valeur(s) Elem : " << *elem.ELPGetValeurElem() << endl;


	return out;
}
#pragma once
#ifndef CSommet
#define CSommetH 5

#include "CListe.h"
#include "CArc.h"

class CSommet
{
	private:

		int iSOMNumSommet;
		CListe<CArc> *pLISArcPartants;
		CListe<CArc> *pLISArcArrivants;
		

	public:

		/**************************************
		**** Constructeur de confort
		***************************************
		**** Entr�e : iNum : int : le num�ro du sommet
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Initialise un objet CSommet en attribuant � son num�ro
					l'entier iNum et en initialisant ses listes d'arcs partants et arrivants
		**** Exception si : N�ant
		**************************************/
		CSommet(int iNum);


		/**************************************
		**** Constructeur de recopie
		***************************************
		**** Entr�e : SOMparam : CSommet : le sommet source
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Intialise un objet CSommet par recopie du num�ro et de la
			liste des arcs partants et arrivants de l'objet source SOMparam
		**** Exception si : N�ant
		**************************************/
		CSommet(CSommet &SOMparam);

		/**************************************
		**** Destructeur
		***************************************
		**** Entr�e : N�ant
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Lib�re l'espace m�moire occup� par pLISArcPartants et pLISArcArrivants
							gr�ce � l'op�rateur delete
		**** Exception si : N�ant
		**************************************/
		~CSommet();
	

		

		/**************************************
		**** Getter de la taille de la liste des arcs partants du sommet en cours
		***************************************
		**** Entr�e : N�ant
		**** N�cessite : N�ant
		**** Sortie : unsigned int
		**** Entraine : Retourne la taille de pLISArcPartants
		**** Exception si : N�ant
		**************************************/
		unsigned int SOMgetTailleArcsPartants();



		/**************************************
		**** Getter de la taille de la liste des arcs arrivants du sommet en cours
		***************************************
		**** Entr�e : N�ant
		**** N�cessite : N�ant
		**** Sortie : unsigned int
		**** Entraine : Retourne la taille de pLISArcArrivants
		**** Exception si : N�ant
		**************************************/
		unsigned int SOMgetTailleArcsArrivants();



		/**************************************
		**** Getter de la liste des arcs partants du sommet en cours
		***************************************
		**** Entr�e : N�ant
		**** N�cessite : N�ant
		**** Sortie : CListe<CArc>*
		**** Entraine : Retourne la liste des arcs partants pLISArcPartants
		**** Exception si : N�ant
		**************************************/
		CListe<CArc>* SOMGetListeArcPartants();



		/**************************************
		**** Getter de la liste des arcs arrivants du sommet en cours
		***************************************
		**** Entr�e : N�ant
		**** N�cessite : N�ant
		**** Sortie : CListe<CArc>*
		**** Entraine : Retourne la liste des arcs arrivants pLISArcArrivants
		**** Exception si : N�ant
		**************************************/
		CListe<CArc>* SOMGetListeArcArrivants();



		/**************************************
		**** Getter de arcs arrivant dont la destination est iDest
		***************************************
		**** Entr�e : iDest : int : la destination de l'arc
		**** N�cessite : N�ant
		**** Sortie : CArc*
		**** Entraine : Retourne un pointeur sur l'arc arrivant dont la destination est iDest
		**** Exception si : N�ant
		**************************************/
		CArc* SOMgetArcArrivant(int iDest);



		/**************************************
		**** Getter de arcs partant dont la destination est iDest
		***************************************
		**** Entr�e : iDest : int : la destination de l'arc
		**** N�cessite : N�ant
		**** Sortie : CArc*
		**** Entraine : Retourne un pointeur sur l'arc partant dont la destination est iDest
		**** Exception si : N�ant
		**************************************/
		CArc* SOMgetArcPartant(int iDest);



		/**************************************
		**** Getter du num�ro du sommet en cours
		***************************************
		**** Entr�e : iDest : int : la destination de l'arc
		**** N�cessite : N�ant
		**** Sortie : int
		**** Entraine : Retourne le num�ro du sommet en cours
		**** Exception si : N�ant
		**************************************/
		int SOMgetNumeroSommet();



		/**************************************
		**** Setter du num�ro du sommet en cours
		***************************************
		**** Entr�e : iNum : int : Le nouveau num�ro du sommet
		**** N�cessite : N�ant
		**** Sortie : int
		**** Entraine : Remplace le num�ro du sommet en cours par iNum
		**** Exception si : N�ant
		**************************************/
		void SOMsetNumeroSommet(int iNum);



		/**************************************
		**** M�thode d'ajout d'un arc partant dans la liste pLISArcPartants du sommet en cours
		***************************************
		**** Entr�e : pARCparam : CArc* : Pointeur sur l'arc � ajouter
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Ajoute l'objet CArc point� par pARCparam dans pLISArcPartants
		**** Exception si : N�ant
		**************************************/
		void SOMAjouterArcPartant(CArc* pARCparam);


		/**************************************
		**** M�thode d'ajout d'un arc partant dans la liste pLISArcArrivants du sommet en cours
		***************************************
		**** Entr�e : pARCparam : CArc* : Pointeur sur l'arc � ajouter
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Ajoute l'objet CArc point� par pARCparam dans pLISArcArrivants
		**** Exception si : N�ant
		**************************************/
		void SOMAjouterArcArrivant(CArc* pARCparam);


		



};



#endif // !CSommet




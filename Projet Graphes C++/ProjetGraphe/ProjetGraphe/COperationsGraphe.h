#pragma once
#ifndef COperationsGraphe
#define CGrapheH 6
#include "CGraphe.h"



class COperationsGraphe
{
public:

	/*******************************************************************
	**** Methode permettant d'inverser les graphes
	********************************************************************
	**** Entr�e: pointeur vers un CGraphe
	**** N�cessite : Un CGraphe non vide
	**** Sortie : pointeur vers un nouveau CGraphe
	**** Entraine : Cr�� un nouveau graphe qui est l'inverse du premier
	**** Exception si : N�ant
	*******************************************************************/
	static CGraphe* OPGInverserGraphe(CGraphe* pGRAparam);

};




#endif // !1




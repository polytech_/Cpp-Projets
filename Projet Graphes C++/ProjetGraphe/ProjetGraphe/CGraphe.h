#pragma once
#ifndef CGraphe
#define CGrapheH 3

#include "CListe.h"
#include "CSommet.h"
#include "CArc.h"
#include "CExeption.h"
#include <iostream>
#include "CParseur.h"

using namespace std;

#define SOMMET_DEJA_EXISTANT 100
#define SOMMET_NON_EXISTANT 101
#define ARC_DEJA_EXISTANT 102
#define ARC_NON_EXISTANT 103




class CGraphe
{
	private :
		
		CListe<CSommet>* pLISGRASommets;


		/**************************************
		**** M�thode de v�rification si un arc existe
		***************************************
		**** Entr�e: Num�ro du sommet de d�part : iSommetDepart et num�ro du sommet
							d'arriv�e : iSommetArrivee
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Retourne true si l'arc existe et false sinon
		**** Exception si : N�ant
		**************************************/
		bool GRAVerifArcExist(int iSommetDepart, int iSommetArrivee);



		/**************************************
		**** M�thode de v�rification si un sommet existe
		***************************************
		**** Entr�e: Num�ro du sommet : iNumSommet
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Retourne true si le sommet existe et false sinon
		**** Exception si : N�ant
		**************************************/
		bool GRAVerifSommetExist(int iNumSommet);


		 int GRAExtraireValeurSommet(char* pcligne);

		 int GRAExtraireValeur1Arc(char* pcligne);

		 int GRAExtraireValeur2Arc(char* pcligne);

		

	public:

		//Constructeurs et destructeur


		/**************************************
		**** Constructeur par d�faut
		***************************************
		**** Entr�e: N�ant
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Met l'attribut pLISGRASommets � nullptr
		**** Exception si : N�ant
		**************************************/
		CGraphe();



		/**************************************
		**** Constructeur de confort
		***************************************
		**** Entr�e: pcFichier : char*
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Cr�e un objet CGraphe correspondant 
				au fichier texte pass� en param�tre
		**** Exception si : 



		**************************************/
		CGraphe(char* pcFichier);



		/**************************************
		**** Destructeur
		***************************************
		**** Entr�e: N�ant
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : lib�re l'espace m�moire de l'attribut pLISGRASommets
						gr�ce � l'op�rateur delete
		**** Exception si : N�ant
		**************************************/
		~CGraphe();



		/**************************************
		**** Constructeur de recopie
		***************************************
		**** Entr�e: GRAparam : CGraphe
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Cr�e un objet CGraphe par recopie des �lements de
						l'objet CGraphe pass� en param�tre
		**** Exception si : N�ant
		**************************************/
		CGraphe(CGraphe &GRAparam);

		

		/**************************************
		**** Getter de la liste des sommets
		***************************************
		**** Entr�e: N�ant
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Retourne l'attribut pLISGRASommets de l'objet
						CGraphe en cours
		**** Exception si : N�ant
		**************************************/
		CListe<CSommet>* GRAgetSommets();


		//Gestion des sommets


		/**************************************
		**** M�thode d'ajout d'un sommet
		***************************************
		**** Entr�e: Le num�ro du sommet que l'on veut ajouter iNumSommet : entier
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Ajoute un sommet dont le num�ro est iNumSommet
						dans le graphe en cours
		**** Exception si : Le sommet existe d�j�
		**************************************/
		void GRAAjouterSommet(int iNumSommet);



		/**************************************
		**** M�thode de modification d'un sommet
		***************************************
		**** Entr�e:  Le num�ro du sommet : iNumSommet : entier et 
					Le num�ro du nouveau sommet : iNumSommet : entier
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Remplace le num�ro de l'ancient sommet par celui du nouveau	
			et ajoute les arcs de l'ancient sommet par ceux du nouveau en �crasant ceux
										de l'ancien
		**** Exception si : Le sommet dont le num�ro est iNumSommet n'existe pas
		**************************************/
		void GRAModifierSommet(int iNumSommet, int iNum);



		/**************************************
		**** M�thode de suppression d'un sommet
		***************************************
		**** Entr�e:  Pointeur sur le sommet que l'on veut supprimer
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Supprime l'objet sommet point� par SOMparam ainsi que ces arcs
		**** Exception si : Le sommet point� par SOMparam n'existe pas
		**************************************/
		void GRASupprimerSommet(CSommet* SOMparam);



		/**************************************
		**** M�thode de retour d'un sommet
		***************************************
		**** Entr�e:  iNumSommet : int : le num�ro du sommet que l'on souhaite retourner
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Retourne un pointeur sur le sommet correspondant au num�ro en param�tre
		**** Exception si : Le sommet n'existe pas
		**************************************/
		CSommet* GRAGetSommet(int iNumSommet);


		//Gestion des arcs


		/**************************************
		**** M�thode d'ajout d'un arc
		***************************************
		**** Entr�e:  Le num�ro du sommet de d�part de l'arc : iSommetDepart : entier et
							Le num�ro du sommet d'arriv�e : iSommetArrivee : entier
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Ajoute un arc partant du sommet correspondant � iSommetDepart et arrivant
								au sommet correspondant � iSommetArrivee
		**** Exception si : L'arc existe d�j� ou si l'un des sommets ou les deux n'existe(ent) pas 
		**************************************/
		void GRAAjouterArc(int iSommetDepart, int  iSommetArrivee);




		/**************************************
		**** M�thode de suppression d'un arc
		***************************************
		**** Entr�e:  Le num�ro du sommet de d�part de l'arc : iSommetDepart : entier et
							Le num�ro du sommet d'arriv�e : iSommetArrivee : entier
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Supprime l'arc partant du sommet correspondant � iSommetDepart et arrivant
								au sommet correspondant � iSommetArrivee
		**** Exception si : L'arc n'existe pas ou si l'un des sommets ou les deux n'existe(ent) pas
		**************************************/
		void GRASupprimerArc(int iSommetDepart, int iSommetArrivee);




		/**************************************
		**** M�thode de modification d'un arc
		***************************************
		**** Entr�e:  Le num�ro de l'ancier sommet de d�part de l'arc : iSommetDepart : entier et
					  Le num�ro de l'ancien sommet d'arriv�e : iSommetArrivee : entier
					  Le num�ro du nouveau sommet de d�part de l'arc : iNouveauSommetDepart : entier et
					  Le num�ro du nouveau sommet d'arriv�e : iNouveauSommetArrivee : entier

		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Modifie le d�part et la destination de l'arc iSommetDepart->iSommetArrivee
							et les remplace	par iNouveauSommetDepart->iNouveauSommetArrivee
		**** Exception si : L'arc n'existe pas ou si l'un des sommets (anciens ou nouveaux) ou les deux n'existe(ent) pas
		**************************************/
		void GRAModifierArc(int iSommetDepart, int iSommetArrivee, int iNouveauSommetDepart, int iNouveauSommetArrivee);



		
		void operator=(CGraphe &GRAparam);//Non termin�



		/**************************************
		**** M�thode d'inversion d'un graphe
		***************************************
		**** Entr�e:  N�ant
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Inverse l'objet CGraphe en cours
		**** Exception si : N�ant
		**************************************/
		void GRAInverserGraphe(); 



		/**************************************
		**** M�thode d'affichage d'un graphe
		***************************************
		**** Entr�e:  N�ant
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Affiche l'objet CGraphe en cours
		**** Exception si : N�ant
		**************************************/
		void GRAAfficherGraphe();

		
	

};

#endif // !CGraphe




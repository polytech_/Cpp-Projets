#include "CGraphe.h"
#define FICHIER_IMCOMPATIBLE 200




CGraphe::CGraphe()
{
	pLISGRASommets = new CListe<CSommet>();
}

CGraphe::CGraphe(char * pcFichier)
{
	unsigned int uiBoucle = 0;
	unsigned int uiBoucleParam = 0;
	int nbSommet,nbArc;
	CParseur* pPARParseur = new CParseur();
	pLISGRASommets = new CListe<CSommet>();


	pPARParseur->PARLecture(pcFichier);

	for (uiBoucle = 0; uiBoucle < pPARParseur->PARGetListeElement()->LISGetTaille();uiBoucle++ ) {

		switch (uiBoucle)
		{
			//Premi�re ligne : recuperation nombre sommet
		case 0:
			if (strstr(pPARParseur->PARGetListeElement()->LISGetElem(uiBoucle)->ELPGetNomElem(), "NBSommets"))
			{
				nbSommet = atoi(*pPARParseur->PARGetListeElement()->LISGetElem(uiBoucle)->ELPGetValeurElem()->LISGetElem(0));
			}

			else {
				CExeption EXC1(FICHIER_IMCOMPATIBLE);
				throw EXC1;
			}
		
			break;



			//Deuxi�me ligne : r�cup�ration du nombre d'arc
		case 1:
			if (strstr(pPARParseur->PARGetListeElement()->LISGetElem(uiBoucle)->ELPGetNomElem(), "NBArcs"))
			{
				nbArc = atoi(*pPARParseur->PARGetListeElement()->LISGetElem(uiBoucle)->ELPGetValeurElem()->LISGetElem(0));
			}

			else {
				CExeption EXC1(FICHIER_IMCOMPATIBLE);
				throw EXC1;
			}
			

			break;



			//Troii�me ligne : r�cup�ration des sommets
		case 2:
			if (strstr(pPARParseur->PARGetListeElement()->LISGetElem(uiBoucle)->ELPGetNomElem(), "Sommets"))
			{
				for (uiBoucleParam = 0; uiBoucleParam < nbSommet; uiBoucleParam++) {
					if (strstr(*pPARParseur->PARGetListeElement()->LISGetElem(uiBoucle)->ELPGetValeurElem()->LISGetElem(uiBoucleParam), "Numero="))
					{
						try {
							GRAAjouterSommet(GRAExtraireValeurSommet(*pPARParseur->PARGetListeElement()->LISGetElem(uiBoucle)->ELPGetValeurElem()->LISGetElem(uiBoucleParam)));
						}
						catch (CExeption EXCprdMat) {
							cerr << "Sommet  deja existant code numero :" << EXCprdMat.EXCLireNErreur() << endl;
						}
					}

					else {
						CExeption EXC1(FICHIER_IMCOMPATIBLE);
						throw EXC1;
					}
				}
			}

			else {
				CExeption EXC1(FICHIER_IMCOMPATIBLE);
				throw EXC1;
			}


			break;

			//A partir de la 4eme ligne : r�cup�ration des arcs
		case 3:
			if (strstr(pPARParseur->PARGetListeElement()->LISGetElem(uiBoucle)->ELPGetNomElem(), "Arcs"))
			{
				for (uiBoucleParam = 0; uiBoucleParam < nbSommet; uiBoucleParam++) {
					if (strstr(*pPARParseur->PARGetListeElement()->LISGetElem(uiBoucle)->ELPGetValeurElem()->LISGetElem(uiBoucleParam), "Debut="))
					{
						try {
							GRAAjouterArc(GRAExtraireValeur1Arc(*pPARParseur->PARGetListeElement()->LISGetElem(uiBoucle)->ELPGetValeurElem()->LISGetElem(uiBoucleParam)), GRAExtraireValeur2Arc(*pPARParseur->PARGetListeElement()->LISGetElem(uiBoucle)->ELPGetValeurElem()->LISGetElem(uiBoucleParam)));
						}
						catch (CExeption EXCprdMat) {
							cerr << "Arc  deja existant code numero :" << EXCprdMat.EXCLireNErreur() << endl;
						}
					}

					else {
						CExeption EXC1(FICHIER_IMCOMPATIBLE);
						throw EXC1;
					}
				}
			}

			else {
				CExeption EXC1(FICHIER_IMCOMPATIBLE);
				throw EXC1;
			}
			break;



		}

		
	}

	delete pPARParseur;

}


CGraphe::~CGraphe()
{
	delete pLISGRASommets;

}



CGraphe::CGraphe(CGraphe & GRAparam)
{
	unsigned int uiBoucle = 0;
	
	pLISGRASommets = new CListe<CSommet>();

	for (uiBoucle = 0; uiBoucle < GRAparam.pLISGRASommets->LISGetTaille(); uiBoucle++) {
		pLISGRASommets->LISAJouter(new CSommet(*GRAparam.pLISGRASommets->LISGetElem(uiBoucle)));
	}

	

}







CListe<CSommet>* CGraphe::GRAgetSommets()
{
	return pLISGRASommets;
}






void CGraphe::GRAAjouterSommet(int iNumSommet)
{
	unsigned int uiBoucle = 0;
	for (uiBoucle = 0; uiBoucle < pLISGRASommets->LISGetTaille(); uiBoucle++) {
		if (pLISGRASommets->LISGetElem(uiBoucle)->SOMgetNumeroSommet()==iNumSommet)
		{
			CExeption EXCprdMat(SOMMET_DEJA_EXISTANT);
			throw EXCprdMat;
		}
	}
	


	pLISGRASommets->LISAJouter(new CSommet(iNumSommet));
	
	
}






void CGraphe::GRAModifierSommet(int iNumSommet, int iNum)
{
	unsigned int uiBoucleCheck = 0;
	for (uiBoucleCheck = 0; uiBoucleCheck < pLISGRASommets->LISGetTaille(); uiBoucleCheck++) {
		if (pLISGRASommets->LISGetElem(uiBoucleCheck)->SOMgetNumeroSommet() == iNumSommet)
		{
			CExeption EXCprdMat(SOMMET_DEJA_EXISTANT);
			throw EXCprdMat;
		}
	}
	
	unsigned int uiBoucle = 0;
	unsigned int uiBoucleArrivants = 0;
	unsigned int uiBouclePartants = 0;

	
	CSommet * pSOMsommetAModifier = GRAGetSommet(iNumSommet);

	CSommet * pSOMsommetAssocie = nullptr;


	//Parcours des arcs arrivants du sommet 
	for (uiBoucleArrivants = 0; uiBoucleArrivants < pSOMsommetAModifier->SOMgetTailleArcsArrivants(); uiBoucleArrivants++)
	{
		//On accede � au sommet associ� � la destination de l'arc parcouru
		pSOMsommetAssocie = GRAGetSommet(pSOMsommetAModifier->SOMGetListeArcArrivants()->LISGetElem(uiBoucleArrivants)->ARCGetDestination());
		//On modifie la valeur  l'arc partant concern�
		pSOMsommetAssocie->SOMgetArcPartant(pSOMsommetAModifier->SOMgetNumeroSommet())->ARCSetDestination(iNum);


	}

	//Parcours des arcs partants du sommet 
	for (uiBouclePartants = 0; uiBouclePartants < pSOMsommetAModifier->SOMgetTailleArcsPartants(); uiBouclePartants++)
	{
		//On accede � au sommet associ� � la destination de l'arc parcouru
		pSOMsommetAssocie = GRAGetSommet(pSOMsommetAModifier->SOMGetListeArcPartants()->LISGetElem(uiBouclePartants)->ARCGetDestination());
		//On modifie la valeur  l'arc arrivant concern�
		pSOMsommetAssocie->SOMgetArcArrivant(pSOMsommetAModifier->SOMgetNumeroSommet())->ARCSetDestination(iNum);


	}

	pSOMsommetAModifier->SOMsetNumeroSommet(iNum);




}



void CGraphe::GRASupprimerSommet(CSommet* SOMparam)
{

	if (!GRAVerifSommetExist(SOMparam->SOMgetNumeroSommet()))
	{
		CExeption EXCprdMat(SOMMET_NON_EXISTANT);
		throw EXCprdMat;
	}


	unsigned int uiBoucle = 0;
	unsigned int uiBouclePartants = 0;
	unsigned int uiBoucleArrivants = 0;
	CSommet * pSOMsommetEnCours=nullptr;


	//Parcours des arcs partants du sommet 
	for (uiBouclePartants = 0; uiBouclePartants < SOMparam->SOMgetTailleArcsPartants(); uiBouclePartants++)
	{
		//On accede � au sommet associ� � la destination de l'arc parcouru
		pSOMsommetEnCours = GRAGetSommet(SOMparam->SOMGetListeArcPartants()->LISGetElem(uiBouclePartants)->ARCGetDestination());
		//On supprime de sa liste des arcs arrivant l'arc concern�
		pSOMsommetEnCours->SOMGetListeArcArrivants()->LISSupprimerElem(pSOMsommetEnCours->SOMgetArcArrivant(SOMparam->SOMgetNumeroSommet()));
	
	}

	//**M�me chose pour les arcs arrivants**//

	//Parcours des arcs arrivants du sommet 
	for (uiBoucleArrivants = 0; uiBoucleArrivants < SOMparam->SOMgetTailleArcsArrivants(); uiBoucleArrivants++)
	{
		//On accede � au sommet associ� � la destination de l'arc parcouru
		pSOMsommetEnCours = GRAGetSommet(SOMparam->SOMGetListeArcArrivants()->LISGetElem(uiBoucleArrivants)->ARCGetDestination());
		//On supprime de sa liste des arcs partants l'arc concern�
		pSOMsommetEnCours->SOMGetListeArcPartants()->LISSupprimerElem(pSOMsommetEnCours->SOMgetArcPartant(SOMparam->SOMgetNumeroSommet()));


	}
	
	pLISGRASommets->LISSupprimerElem(SOMparam);
	//break;

}

	



CSommet * CGraphe::GRAGetSommet(int iNumSommet)
{
	unsigned int uiBoucleCheck = 0;
	bool bExist = false;
	for (uiBoucleCheck = 0; uiBoucleCheck < pLISGRASommets->LISGetTaille(); uiBoucleCheck++) {
		if (pLISGRASommets->LISGetElem(uiBoucleCheck)->SOMgetNumeroSommet() == iNumSommet)
		{
			bExist = true;
		}
	}
	if (!bExist)
	{
		CExeption EXCprdMat(SOMMET_NON_EXISTANT);
		throw EXCprdMat;
		return nullptr;
	}

	else {
		unsigned int uiBoucle = 0;
		for (uiBoucle = 0; uiBoucle < pLISGRASommets->LISGetTaille(); uiBoucle++) {
			if (pLISGRASommets->LISgetListe()[uiBoucle]->SOMgetNumeroSommet() == iNumSommet) {


				return pLISGRASommets->LISgetListe()[uiBoucle];
				//break;

			}
		}

	}

}





void CGraphe::GRAAjouterArc(int iSommetDepart, int iSommetArrivee)
{
	
	if (GRAVerifArcExist(iSommetDepart, iSommetArrivee))
	{
		CExeption EXCprdMat(ARC_DEJA_EXISTANT);
		throw EXCprdMat;
	}
	
	CArc* pARCdep = new CArc(iSommetArrivee);
	CArc* pARCarr = new CArc(iSommetDepart);
	CSommet * pSOMsommetArrivee = nullptr;

	//Eventuellement creer methode privee verifArcExist() et lever une exception si l'arc existe deja


	CSommet * pSOMsommetDepart = GRAGetSommet(iSommetDepart);
	if (pSOMsommetDepart != nullptr)
		pSOMsommetDepart->SOMAjouterArcPartant(pARCdep);
	else
	{
		CExeption EXCprdMat(SOMMET_NON_EXISTANT);
		throw EXCprdMat;
	}


	 pSOMsommetArrivee = GRAGetSommet(iSommetArrivee);
	if (pSOMsommetArrivee != nullptr)
		pSOMsommetArrivee->SOMAjouterArcArrivant(pARCarr);
	else
	{
		CExeption EXCprdMat(SOMMET_NON_EXISTANT);
		throw EXCprdMat;
	}
	


}



void CGraphe::GRASupprimerArc(int iSommetDepart, int iSommetArrivee)
{
	CSommet * pSOMsommetDepart = GRAGetSommet(iSommetDepart);
	
	if (!GRAVerifArcExist(iSommetDepart, iSommetArrivee))
	{
		CExeption EXCprdMat(ARC_NON_EXISTANT);
		throw EXCprdMat;
		
	}


	unsigned int uiBoucle = 0;

	//CSommet * pSOMsommetDepart = nullptr;

	

	//Supprimer si l'arc existe sinon exception (grace a la methode priv�e verifArcExist)
	if (pSOMsommetDepart != nullptr) {
		pSOMsommetDepart->SOMGetListeArcPartants()->LISSupprimerElem(pSOMsommetDepart->SOMgetArcPartant(iSommetArrivee));
	}
		
	else
	{
		//Exception a lever
	}

	CSommet * pSOMsommetArrivee = GRAGetSommet(iSommetArrivee);
	if (pSOMsommetArrivee != nullptr)
		pSOMsommetArrivee->SOMGetListeArcArrivants()->LISSupprimerElem(pSOMsommetArrivee->SOMgetArcArrivant(iSommetDepart));
	else
	{
		//Exception a lever
	}
	
}



void CGraphe::GRAModifierArc(int iSommetDepart, int iSommetArrivee,int iNouveauSommetDepart,int iNouveauSommetArrivee)
{
	if (!GRAVerifSommetExist(iSommetDepart) && !GRAVerifSommetExist(iSommetArrivee)
		&& !GRAVerifSommetExist(iNouveauSommetDepart) && !GRAVerifSommetExist(iNouveauSommetArrivee))
	{
		CExeption EXCprdMat(SOMMET_NON_EXISTANT);
		throw EXCprdMat;

	}

	
	if (!GRAVerifArcExist(iSommetDepart, iSommetArrivee))
	{
		CExeption EXCprdMat(ARC_NON_EXISTANT);
		throw EXCprdMat;

	}


	//Modifier si l'arc ancien existe sinon exception (grace a la methode priv�e verifArcExist)
	//Modifier si l'arc nouveau n'existe pas sinon exception (grace a la methode priv�e verifArcExist)

	GRASupprimerArc(iSommetDepart, iSommetArrivee);
	GRAAjouterArc(iNouveauSommetDepart, iNouveauSommetArrivee);


}




void CGraphe::operator=(CGraphe & GRAparam)
{
	unsigned int uiBoucle = 0;

	unsigned int uiBoucle1 = 0;
	unsigned int uiBoucle2 = 0;

	int * piArcsPartants= nullptr;
	int * piArcsArrivants = nullptr;

	for (uiBoucle = 0; uiBoucle < pLISGRASommets->LISGetTaille(); uiBoucle++) {
		if (pLISGRASommets->LISGetElem(uiBoucle) != nullptr) {
			GRASupprimerSommet(pLISGRASommets->LISGetElem(uiBoucle));
		}
		
	}

	for (uiBoucle = 0; uiBoucle < GRAparam.pLISGRASommets->LISGetTaille(); uiBoucle++) {
			
		GRAAjouterSommet(GRAparam.pLISGRASommets->LISGetElem(uiBoucle)->SOMgetNumeroSommet());
	
	
	
	}

	for (uiBoucle = 0; uiBoucle < GRAparam.pLISGRASommets->LISGetTaille(); uiBoucle++) {
		//Ajout des arcs
	}



}







void CGraphe::GRAInverserGraphe()
{
	
	CListe<int>* a = new CListe<int>();
	CListe<int>* b = new CListe<int>();
	int num = 0;
	for (int i = 0; i < GRAgetSommets()->LISGetTaille(); i++) {
		num = GRAgetSommets()->LISGetElem(i)->SOMgetNumeroSommet();
		for (int j = 0; j < GRAGetSommet(num)->SOMgetTailleArcsPartants(); j++) {
			a->LISAJouter(new int(GRAGetSommet(num)->SOMgetNumeroSommet()));
			b->LISAJouter(new int (GRAGetSommet(num)->SOMGetListeArcPartants()->LISGetElem(j)->ARCGetDestination()));
		}
	}


	for (int i = 0; i < a->LISGetTaille(); i++) {
		GRAModifierArc(*(*a)[i], *(*b)[i], *(*b)[i], *(*a)[i]);
	}

	delete a;
	delete b;
}






void CGraphe::GRAAfficherGraphe()
{
	unsigned int uiBoucleSommet = 0;
	unsigned int uiBoucleArcP = 0;
	unsigned int uiBoucleArcA = 0;
	int numSommet = 0;

	for (uiBoucleSommet = 0; uiBoucleSommet < GRAgetSommets()->LISGetTaille(); uiBoucleSommet++) {
		numSommet = GRAgetSommets()->LISGetElem(uiBoucleSommet)->SOMgetNumeroSommet();
		cout << "Sommet " << numSommet << endl;
		cout << "	Arcs partants :" << endl;
		for (uiBoucleArcP = 0; uiBoucleArcP < GRAGetSommet(numSommet)->SOMgetTailleArcsPartants(); uiBoucleArcP++) {
			cout << "		- Vers Sommet num : " << GRAGetSommet(numSommet)->SOMGetListeArcPartants()->LISGetElem(uiBoucleArcP)->ARCGetDestination() << endl;

		}
		
		cout << "	Arcs arrivants :" << endl;
		for (uiBoucleArcA = 0; uiBoucleArcA < GRAGetSommet(numSommet)->SOMgetTailleArcsArrivants(); uiBoucleArcA++) {
			cout << "		- Depuis Sommet num : " << GRAGetSommet(numSommet)->SOMGetListeArcArrivants()->LISGetElem(uiBoucleArcA)->ARCGetDestination() << endl;

		}
	}
}





bool CGraphe::GRAVerifArcExist(int iSommetDepart, int iSommetArrivee)
{
	unsigned int uiBoucle = 0;
	bool bExist1 = false;
	bool bExist2 = false;

	for (uiBoucle = 0; uiBoucle < pLISGRASommets->LISGetTaille(); uiBoucle++) {
		
		//On v�rifie si le premier objet arc existe dans le sommet de d�part
		if(pLISGRASommets->LISGetElem(uiBoucle)->SOMgetNumeroSommet()==iSommetDepart &&
			pLISGRASommets->LISGetElem(uiBoucle)->SOMgetArcPartant(iSommetArrivee)!=nullptr)
		{
			bExist1 = true;
		}

	}

	for (uiBoucle = 0; uiBoucle < pLISGRASommets->LISGetTaille(); uiBoucle++) {
		
		//On v�rifie si le deuxieme objet arc existe dans le sommet d'arriv�e
		if (pLISGRASommets->LISGetElem(uiBoucle)->SOMgetNumeroSommet() == iSommetArrivee &&
			pLISGRASommets->LISGetElem(uiBoucle)->SOMgetArcArrivant(iSommetDepart) != nullptr)
		{
			bExist2 = true;
		}

	}
	
	return bExist1 && bExist2;
}





bool CGraphe::GRAVerifSommetExist(int iNumSommet)
{
	unsigned int uiBoucle= 0;
	bool bExist = false;

	for (uiBoucle = 0; uiBoucle < pLISGRASommets->LISGetTaille(); uiBoucle++) {
		if (pLISGRASommets->LISGetElem(uiBoucle)->SOMgetNumeroSommet() == iNumSommet)
		{
			bExist = true;
		}
	}


	return bExist;
}




 int CGraphe::GRAExtraireValeurSommet(char* pcligne) {

	char chaine[5];
	char carac = pcligne[0];
	unsigned int cpt = 0;
	unsigned int flag = 0;

	while (carac != '\0')
	{
		cpt++;
		carac = pcligne[cpt];

		if (flag > 0)
		{
			chaine[flag - 1] = pcligne[cpt];
			flag++;
		}

		if (carac == '=')
		{
			flag = 1;
		}


	}

	return ( int)atoi(chaine);

}


 int CGraphe::GRAExtraireValeur1Arc(char* pcligne) {

	 char chaine[5];
	 char carac = pcligne[0];
	 unsigned int cpt = 0;
	 unsigned int flag = 0;

	 while (carac != ',')
	 {
		 cpt++;
		 carac = pcligne[cpt];
		 if (carac != ',')
		 {
			 if (flag > 0)
			 {
				 chaine[flag - 1] = pcligne[cpt];
				 flag++;
			 }

			 if (carac == '=')
			 {
				 flag = 1;
			 }
		 }


	 }

	 chaine[flag - 1] = '\0';
	 return (int)atoi(chaine);

 }


 int CGraphe::GRAExtraireValeur2Arc(char* pcligne) {


	 char chaine[5] = "";
	 char carac = pcligne[0];
	 unsigned int cpt = 0;
	  int flag = -1;

	 while (carac != '\0')
	 {
		 cpt++;
		 carac = pcligne[cpt];

		 if (flag > 0)
		 {
			 chaine[flag - 1] = pcligne[cpt];
			 flag++;
		 }

		 if (carac == '=')
		 {
			 flag ++;
		 }


	 }

	

	 return (int)atoi(chaine);

 }




// ProjetGraphe.cpp : This file contains the 'main' function. Program execution begins and ends there.
//


#include <iostream>
#include "CGraphe.h"
#include "COperationsGraphe.h"
#include <stdio.h>

using namespace std;

int main(int argc, char *argv[])
{

	char*  chaine = argv[1];
	CGraphe *pGRAgrapheInverse;
	CGraphe *pGRAgraphe = new CGraphe(chaine);

	cout << "Affichage du graphe du fichier : " << chaine << endl;
	pGRAgraphe->GRAAfficherGraphe();

	pGRAgrapheInverse = COperationsGraphe::OPGInverserGraphe(pGRAgraphe);
	cout << "----------------------------------------------------------------------------"  << endl;
	cout << "Affichage du graphe inverse : "  << endl;
	pGRAgrapheInverse->GRAAfficherGraphe();

	delete pGRAgraphe;
	delete pGRAgrapheInverse;

	
	/*
	try {
		GRAgraphe->GRAAjouterSommet(1);
	}
	catch (CExeption EXCprdMat) {
		cerr << "Sommet 1 deja existant code numero :" << EXCprdMat.EXCLireNErreur() << endl;
	}

	try {
		GRAgraphe->GRAAjouterSommet(2);
	}
	catch (CExeption EXCprdMat) {
		cerr << "Sommet 2 deja existant code numero : " << EXCprdMat.EXCLireNErreur() << endl;
	}

	try {
		GRAgraphe->GRAAjouterSommet(3);
	}
	catch (CExeption EXCprdMat) {
		cerr << "Sommet 3 deja existant code numero :" << EXCprdMat.EXCLireNErreur() << endl;
	}

	try {
		GRAgraphe->GRAAjouterSommet(4);
	}
	catch (CExeption EXCprdMat) {
		cerr << "Sommet 4 deja existant code numero : " << EXCprdMat.EXCLireNErreur() << endl;
	}
	



	try {
		GRAgraphe->GRAAjouterArc(1, 2);
	}
	catch (CExeption EXCprdMat) {
		cerr << "Arc 1->2 deja dexistant code numero : " << EXCprdMat.EXCLireNErreur() << endl;
	}

	try {
		GRAgraphe->GRAAjouterArc(2, 4);
	}
	catch (CExeption EXCprdMat) {
		cerr << "Arc 2->4 deja existant code numero : " << EXCprdMat.EXCLireNErreur() << endl;
	}

	try {
		GRAgraphe->GRAAjouterArc(2, 3);
	}
	catch (CExeption EXCprdMat) {
		cerr << "Arc 2->3 deja existant code numero : " << EXCprdMat.EXCLireNErreur() << endl;
	}

	try {
		GRAgraphe->GRAAjouterArc(4, 3);
	}
	catch (CExeption EXCprdMat) {
		cerr << "Arc 4->3 deja existant code numero : " << EXCprdMat.EXCLireNErreur() << endl;
	}

	/*try {
		GRAgraphe->GRAAjouterArc(4, 3);
	}
	catch (CExeption EXCprdMat) {
		cerr << "Arc 4->3 deja existant code numero : " << EXCprdMat.EXCLireNErreur() << endl;
	}
	
	try {
		GRAgraphe->GRAAjouterArc(2, 4);
	}
	catch (CExeption EXCprdMat) {
		cerr << "Arc 2->4 deja existant code numero : " << EXCprdMat.EXCLireNErreur() << endl;
	}
	*/
	
	

	

	

/*	try {
		GRAgraphe->GRASupprimerSommet(GRAgraphe->GRAGetSommet(4));
	}
	catch (CExeption EXCprdMat) {
		cerr << "Sommet 4 non existant code numero : " << EXCprdMat.EXCLireNErreur() << endl;
	}
	
	/*try {
		GRAgraphe->GRASupprimerArc(1, 2);
	}
	catch (CExeption EXCprdMat) {
		cerr << "Arc 1->2 non existant code numero : " << EXCprdMat.EXCLireNErreur() << endl;
	}*/

	/*try {
		GRAgraphe->GRAModifierArc(1, 2, 25, 3);
	}
	catch (CExeption EXCprdMat) {
		cerr << "Arc 1->2 introuvable ou sommet(s) non existant(s) code numero : " << EXCprdMat.EXCLireNErreur() << endl;
	}
	*/


	

	/*cout << "GRAPHE 1 :\n" << endl;

	//GRAgraphe->GRAInverserGraphe();
	GRAgraphe->GRAAfficherGraphe();
	
	//delete GRAgraphe;
	

	CParseur* prs1 = new CParseur();
	prs1->PARLecture("graph1.txt");

	for (int i = 0; i < prs1->PARGetListeElement()->LISGetTaille(); i++)
	{
		cout << "NOM :" <<prs1->PARGetListeElement()->LISGetElem(i)->ELPGetNomElem() << endl;
		cout << "VALEUR :";
		for(int j=0;j< prs1->PARGetListeElement()->LISGetElem(i)->ELPGetValeurElem()->LISGetTaille();j++)
			cout << " "<< *prs1->PARGetListeElement()->LISGetElem(i)->ELPGetValeurElem()->LISGetElem(j) << endl;

	}
	//cout << *prs1->PARGetListeElement() << endl;



	//cout << prs1->PARgetElementGauche(chaine) << endl;

	*/














	/*
	delete a;
	delete b;
	*/
/*	cout << "GRAPHE 2 :" << endl;
	CGraphe *GRAgraphe2 = new CGraphe(*GRAgraphe);

	GRAgraphe2->GRASupprimerSommet(GRAgraphe2->GRAGetSommet(2));

	GRAgraphe2->GRAModifierSommet(1, 8);

	GRAgraphe2->GRAAfficherGraphe();


	delete GRAgraphe2;
	*/


	/*
	CListe<int> *liste = new CListe<int>();

	int *a = new int[1];
	*a = 1;
	int *b = new int[1];
	*b = 2;
	int *c = new int[1];
	*c = 3;
	int *d = new int[1];
	*d = 4;
	int *e = new int[1];
	*e = 5;
	int *f = new int[1];
	*f = 6;
	liste->LISAJouter(a);
	liste->LISAJouter(b);
	liste->LISAJouter(c);
	liste->LISAJouter(d);
	liste->LISAJouter(e);
	liste->LISAJouter(f);

	for (int i = 0; i < liste->LISGetTaille(); i++) {
		cout << "[" << *(*liste)[i] << "] ";
	}

	liste->LISSupprimerElem(a);
	liste->LISSupprimerElem(c);
	liste->LISSupprimerElem(f);
	cout<<endl;
	for (int i = 0; i < liste->LISGetTaille(); i++) {
		cout << "[" << *(*liste)[i] << "] ";
	}
	*/



}



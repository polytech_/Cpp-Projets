#include "CSommet.h"


CSommet::CSommet(int iNum)
{
	iSOMNumSommet = iNum;
	pLISArcArrivants = new CListe<CArc>();
	pLISArcPartants = new CListe<CArc>();

}




CSommet::CSommet(CSommet & SOMparam)
{
	unsigned int uiBoucle = 0;

	pLISArcPartants = new CListe<CArc>();

	iSOMNumSommet = SOMparam.SOMgetNumeroSommet();

	for (uiBoucle = 0; uiBoucle < SOMparam.pLISArcPartants->LISGetTaille(); uiBoucle++) {
		pLISArcPartants->LISAJouter(new CArc(SOMparam.SOMGetListeArcPartants()->LISGetElem(uiBoucle)->ARCGetDestination()));
	}

	pLISArcArrivants = new CListe<CArc>();

	for (uiBoucle = 0; uiBoucle < SOMparam.pLISArcArrivants->LISGetTaille(); uiBoucle++) {
		pLISArcArrivants->LISAJouter(new CArc(SOMparam.SOMGetListeArcArrivants()->LISGetElem(uiBoucle)->ARCGetDestination()));
	}

}




CSommet::~CSommet()
{
	delete pLISArcArrivants;
	delete pLISArcPartants;
}






CListe<CArc>* CSommet::SOMGetListeArcArrivants() {

	return pLISArcArrivants;

}






unsigned int CSommet::SOMgetTailleArcsPartants()
{
	return pLISArcPartants->LISGetTaille();
}





unsigned int CSommet::SOMgetTailleArcsArrivants()
{
	return pLISArcArrivants->LISGetTaille();
}




CListe<CArc>* CSommet::SOMGetListeArcPartants() {

	return pLISArcPartants;

}




CArc * CSommet::SOMgetArcArrivant(int iDest)
{
	unsigned int uiBoucle = 0;
	for (uiBoucle = 0; uiBoucle < SOMgetTailleArcsArrivants(); uiBoucle++) {
		if (pLISArcArrivants->LISGetElem(uiBoucle)->ARCGetDestination() == iDest) {
			return pLISArcArrivants->LISGetElem(uiBoucle);
		}
	}
	return nullptr;
}




CArc * CSommet::SOMgetArcPartant(int iDest)
{
	unsigned int uiBoucle = 0;
	for (uiBoucle = 0; uiBoucle < SOMgetTailleArcsPartants(); uiBoucle++) {
		if (pLISArcPartants->LISGetElem(uiBoucle)->ARCGetDestination() == iDest) {
			return pLISArcPartants->LISGetElem(uiBoucle);
		}
	}
	return nullptr;
}


int CSommet::SOMgetNumeroSommet() {
	return iSOMNumSommet;
}


void CSommet::SOMsetNumeroSommet(int iNum)
{
	iSOMNumSommet = iNum;
}

void CSommet::SOMAjouterArcPartant(CArc * pARCparam)
{
	pLISArcPartants->LISAJouter(pARCparam);
}


void CSommet::SOMAjouterArcArrivant(CArc * pARCparam)
{
	pLISArcArrivants->LISAJouter(pARCparam);
}




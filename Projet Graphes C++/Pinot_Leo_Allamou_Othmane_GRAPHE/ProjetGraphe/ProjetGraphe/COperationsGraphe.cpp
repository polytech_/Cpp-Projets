#include "COperationsGraphe.h"





CGraphe * COperationsGraphe::OPGInverserGraphe(CGraphe * pGRAparam)
{
	CGraphe* GRAInverse = new CGraphe(*pGRAparam);

	CListe<int>* a = new CListe<int>();
	CListe<int>* b = new CListe<int>();
	int num = 0;
	for (int i = 0; i < GRAInverse->GRAgetSommets()->LISGetTaille(); i++) {
		num = GRAInverse->GRAgetSommets()->LISGetElem(i)->SOMgetNumeroSommet();
		for (int j = 0; j < GRAInverse->GRAGetSommet(num)->SOMgetTailleArcsPartants(); j++) {
			a->LISAJouter(new int(GRAInverse->GRAGetSommet(num)->SOMgetNumeroSommet()));
			b->LISAJouter(new int(GRAInverse->GRAGetSommet(num)->SOMGetListeArcPartants()->LISGetElem(j)->ARCGetDestination()));
		}
	}


	for (int i = 0; i < a->LISGetTaille(); i++) {
		GRAInverse->GRAModifierArc(*(*a)[i], *(*b)[i], *(*b)[i], *(*a)[i]);
	}

	delete a;
	delete b;

	return GRAInverse;
}
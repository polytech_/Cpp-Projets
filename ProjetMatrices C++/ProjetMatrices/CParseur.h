#pragma once
#ifndef CParseurH 
#define CParseurH 8
#include "CListe.h"
#include "CElementParseur.h"
#include "CExeption.h"

#include <iostream>
#include <fstream>
#include <string>

using namespace std;



class CParseur
{
private:


	/**************************************
	**** Destructeur
	***************************************
	**** Entr�e: N�ant
	**** N�cessite : N�ant
	**** Sortie : N�ant
	**** Entraine : N�ant
	**** Exception si : N�ant
	**************************************/
	CListe<CElementParseur>* pLISPARElementsParseurs;



	/**************************************
	**** M�thode de v�rification si la ligne suivante contient les �lements d'un sommet ou d'un arc
	***************************************
	**** Entr�e: pcLigne : la ligne en cours du fichier
	**** N�cessite : N�ant
	**** Sortie : bool
	**** Entraine : Retourne vrai si la ligne pcLigne finit par "[" et false sinon
	**** Exception si : N�ant
	**************************************/
	bool bPARElemEstTableau(char * pcLigne);


	/**************************************
	**** M�thode d'extraction de la valeur qui vient apr�s le "=" dans le fichier texte
	***************************************
	**** Entr�e: pcLigne : la ligne en cours du fichier
	**** N�cessite : N�ant
	**** Sortie : char**
	**** Entraine : Retourne l'adresse de la chaine de caract�re contenant ce qui vient apr�s le symbole "="
	**** Exception si : N�ant
	**************************************/
	char * PARExtraireValeur(char * pcligne);


	/**************************************
	**** M�thode de copie d'une chaine de caractere ver une nouvelle
	***************************************
	**** Entr�e: pcLigne : la ligne en cours du fichier
	**** N�cessite : N�ant
	**** Sortie : char *
	**** Entraine : copie la chaine en entr�e vers une nouvelle chaine allou� en m�moire
	**** Exception si : N�ant
	**************************************/
	char * PARCopyLigne(char * pcLigne);


	/**************************************
	**** M�thode d'extraction de ce qui vient avant le "=" dans le fichier texte
	***************************************
	**** Entr�e: pcLigne : la ligne en cours du fichier
	**** N�cessite : N�ant
	**** Sortie : char**
	**** Entraine : Retourne la chaine de caract�re contenant ce qui vient avant le symbole "="
	**** Exception si : N�ant
	**************************************/
	char * PARgetElementGauche(char * pcLigne);


public:




	/**************************************
	**** Constructeur par d�faut
	***************************************
	**** Entr�e: N�ant
	**** N�cessite : N�ant
	**** Sortie : N�ant
	**** Entraine : N�ant
	**** Exception si : N�ant
	**************************************/
	CParseur();



	/**************************************
	**** Destructeur
	***************************************
	**** Entr�e: N�ant
	**** N�cessite : N�ant
	**** Sortie : N�ant
	**** Entraine : N�ant
	**** Exception si : N�ant
	**************************************/
	~CParseur();



	/**************************************
	**** M�thode de parsage d'un fichier texte afin de r�cup�rer les �lements de gauche et de droite
	***************************************
	**** Entr�e: pcFichier : const char*
	**** N�cessite : N�ant
	**** Sortie : CListe<CElementParseur>*
	**** Entraine : Cr�ation d'un objet CListe<CElementParseur>* contenant
				les noms (Sommets=[, Arcs=[, Debut=, Fin=) et les valeurs (ce qui vient � droite du = )
	**** Exception si :
			->
			-> Fichier introuvable
			-> Fichier incompatible avec le mod�le
	**************************************/
	CListe<CElementParseur>* PARLecture(const char* pcFichier);



	/**************************************
	**** Getter de la liste des �lements
	***************************************
	**** Entr�e: N�ant
	**** N�cessite : N�ant
	**** Sortie : CListe<CElementParseur>*
	**** Entraine : Retourne l'attribut pLISPARElementsParseurs
	**** Exception si : N�ant
	**************************************/
	CListe<CElementParseur>* PARGetListeElement();


};



#endif // !1





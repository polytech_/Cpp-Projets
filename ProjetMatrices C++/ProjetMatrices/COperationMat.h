#pragma once
#include "CMatrice.h"

class COperationMat
{
public:
	

	/**************************************
	**** M�thode permettant de retourner la transpos�e d'une matrice
	***************************************
	**** Entr�e: CMatrice<Type> MATparam
	**** N�cessite : N�ant
	**** Sortie : pointeur vers CMatrice<Type> 
	**** Entraine : Cr�ation et retour de la transpos�e de l'objet CMatrice en param�tre
	**** Exception si : N�ant
	**************************************/
	template <class Type> static CMatrice<Type>  OPMTransposee(CMatrice<Type> MATparam);
};

template<class Type> inline CMatrice<Type> COperationMat::OPMTransposee(CMatrice<Type> MATparam)
{

	unsigned int uiBouclei = 0;
	unsigned int uiBouclej = 0;


	Type** pTTresultat = new Type*[MATparam.MATgetNbColonnes()];



	for (uiBouclei; uiBouclei < MATparam.MATgetNbColonnes(); uiBouclei++) {
		pTTresultat[uiBouclei] = new Type[MATparam.MATgetNbLignes()];
		for (uiBouclej = 0; uiBouclej < MATparam.MATgetNbLignes(); uiBouclej++) {

			pTTresultat[uiBouclei][uiBouclej] = MATparam.getMatrice()[uiBouclej][uiBouclei];

		}

	}

	CMatrice<Type> MATres(MATparam.MATgetNbLignes(), MATparam.MATgetNbColonnes(), pTTresultat);

	for (uiBouclei = 0; uiBouclei < MATparam.MATgetNbColonnes(); uiBouclei++) {
		delete pTTresultat[uiBouclei];
	}
	delete pTTresultat;


	return MATres;
}

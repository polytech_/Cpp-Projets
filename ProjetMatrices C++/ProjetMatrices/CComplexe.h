#pragma once
#ifndef CComplexH
#define CComplexH 4
#include <iostream>
#include "CExeption.h"
#include <math.h>
#include <iostream>
#define DENOMINATEUR_CPX_NUL 203
using namespace std;



class CComplexe
{
private:


	double dCPXReelle;


	double dCPXImaginaire;


public:

	/**************************************
	**** Constructeur par d�faut
	***************************************
	**** Entr�e: N�ant
	**** N�cessite : N�ant
	**** Sortie : N�ant
	**** Entraine : N�ant.
	**** Exception si : N�ant
	**************************************/
	CComplexe();


	/**************************************
	**** Constructeur de confort
	***************************************
	**** Entr�e: dreelle : double, dimaginaire : double
	**** N�cessite : N�ant
	**** Sortie : N�ant
	**** Entraine : Affecte l'attribut dCPXReelle � dreelle et dCPXImaginaire � dimaginaire
	**** Exception si : N�ant
	**************************************/
	CComplexe(double dreelle, double dimaginaire);

	
	/**************************************
	**** Constructeur de confort
	***************************************
	**** Entr�e: pcNombre : char *
	**** N�cessite : La chaine doit etre de la forme "x.x+y.yi" ou "x.x-y.yi" (exemple :"3.2+12.0i")
	**** Sortie : N�ant
	**** Entraine : Initialise un objet CComplexe � partir de la chaine de caract�res pcNombre
	**** Exception si : N�ant
	**************************************/
	CComplexe(char * pcNombre);

	/**************************************
	**** Constructeur de copie
	***************************************
	**** Entr�e: CPXparam : CComplexe
	**** N�cessite : N�ant
	**** Sortie : N�ant
	**** Entraine : Initialise un objet CComplexe par recopie � partir de l'objet CPXparam
	**** Exception si : N�ant
	**************************************/
	CComplexe(CComplexe &CPXparam);

	/**************************************
	**** Getter de la partie r�elle
	***************************************
	**** Entr�e: N�ant
	**** N�cessite : N�ant
	**** Sortie : double
	**** Entraine : Retourne l'attribut dCPXReelle de l'objet en cours
	**** Exception si : N�ant
	**************************************/
	double CPXgetPartieReelle();

	/**************************************
	**** Getter de la partie imaginaire
	***************************************
	**** Entr�e: N�ant
	**** N�cessite : N�ant
	**** Sortie : double
	**** Entraine : Retourne l'attribut dCPXImaginaire de l'objet en cours
	**** Exception si : N�ant
	**************************************/
	double CPXgetPartieImaginaire();
	

	/**************************************
	**** Setter de la partie r�elle
	***************************************
	**** Entr�e: dparam : double
	**** N�cessite : N�ant
	**** Sortie : N�ant
	**** Entraine : Affecte dparam � l'attribut dCPXReelle de l'objet en cours
	**** Exception si : N�ant
	**************************************/
	void CPXsetPartieReelle(double dparam);


	/**************************************
	**** Setter de la partie imaginaire
	***************************************
	**** Entr�e: dparam : double
	**** N�cessite : N�ant
	**** Sortie : N�ant
	**** Entraine : Affecte dparam � l'attribut dCPXImaginaire de l'objet en cours
	**** Exception si : N�ant
	**************************************/
	void CPXsetPartieImaginaire(double dparam);


	/**************************************
	**** Surchage de l'op�rateur +
	***************************************
	**** Entr�e: CPXparam : CComplexe
	**** N�cessite : N�ant
	**** Sortie : CComplexe
	**** Entraine : Ajoute les attributs de l'objet en cours aux attributs de l'objet CPXparam
	**** Exception si : N�ant
	**************************************/
	CComplexe operator+(CComplexe &CPXparam);

	/**************************************
	**** Surchage de l'op�rateur +
	***************************************
	**** Entr�e: dNombre : double
	**** N�cessite : N�ant
	**** Sortie : CComplexe
	**** Entraine : Ajoute dNombre aux attributs de l'objet en cours
	**** Exception si : N�ant
	**************************************/
	CComplexe operator+(double dNombre);



	/**************************************
	**** Surchage de l'op�rateur -
	***************************************
	**** Entr�e: CComplexe : CPXparam
	**** N�cessite : N�ant
	**** Sortie : CComplexe
	**** Entraine : Soustrait les attributs de CPXparam des attributs de l'objet en cours
	**** Exception si : N�ant
	**************************************/
	CComplexe operator-(CComplexe &CPXparam);

	/**************************************
	**** Surchage de l'op�rateur -
	***************************************
	**** Entr�e: dNombre : double
	**** N�cessite : N�ant
	**** Sortie : CComplexe
	**** Entraine : Soustrait dNombre des attributs de l'objet en cours
	**** Exception si : N�ant
	**************************************/
	CComplexe operator-(double dNombre);



	/**************************************
	**** Surchage de l'op�rateur *
	***************************************
	**** Entr�e: CPXparam : CComplexe
	**** N�cessite : N�ant
	**** Sortie : CComplexe
	**** Entraine : Multiplie les attributs de CPXparam par les attributs de l'objet en cours
	**** Exception si : N�ant
	**************************************/
	CComplexe operator*(CComplexe &CPXparam);

	/**************************************
	**** Surchage de l'op�rateur *
	***************************************
	**** Entr�e: dNombre : double
	**** N�cessite : N�ant
	**** Sortie : CComplexe
	**** Entraine : Multiplie les attributs de l'objet en cours par dNombre
	**** Exception si : N�ant
	**************************************/
	CComplexe operator*(double dNombre);


	/**************************************
	**** Surchage de l'op�rateur /
	***************************************
	**** Entr�e: CPXparam : CComplexe
	**** N�cessite : N�ant
	**** Sortie : CComplexe
	**** Entraine : Divise l'objet en cours par CPXparam (divison complexe en passant par le conjugu� de CPXparam)
	**** Exception si : N�ant
	**************************************/
	CComplexe operator/(CComplexe &CPXparam);

	/**************************************
	**** Surchage de l'op�rateur /
	***************************************
	**** Entr�e: dNombre : double
	**** N�cessite : N�ant
	**** Sortie : CComplexe
	**** Entraine : Divise les attributs de l'objet en cours par dNombre
	**** Exception si : dNombre = 0
	**************************************/
	CComplexe operator/(double dNombre);


	/**************************************
	**** Surchage de l'op�rateur =
	***************************************
	**** Entr�e: CPXparam : CComplexe
	**** N�cessite : N�ant
	**** Sortie : N�ant
	**** Entraine : Affecte l'objet CPXparam � l'objet en cours
	**** Exception si : N�ant
	**************************************/
	void operator=(CComplexe &CPXparam);

	/**************************************
	**** Surchage de l'op�rateur =
	***************************************
	**** Entr�e: dNombre : double
	**** N�cessite : N�ant
	**** Sortie : N�ant
	**** Entraine : Affecte dNombre � la partie r�elle de l'objet en cours s'il est non nul, sinon � la partie imaginaire aussi
	**** Exception si : N�ant
	**************************************/
	void operator=(double dNombre);


	/**************************************
	**** Surchage de l'op�rateur +=
	***************************************
	**** Entr�e: CPXparam : CComplexe
	**** N�cessite : N�ant
	**** Sortie : N�ant
	**** Entraine : Augmente la valeur de la partie r�elle de l'objet en cours de CPXparam.dCPXReelle et m�me chose pour la partie imaginaire
	**** Exception si : N�ant
	**************************************/
	CComplexe operator+=(CComplexe &CPXparam);

	/**************************************
	**** M�thode d'affichage qui sera utilis� dans l'op�rateur <<
	***************************************
	**** Entr�e: N�ant
	**** N�cessite : N�ant
	**** Sortie : char *
	**** Entraine : Affiche les attributs de l'objet en cours sous forme de chaine de caract�re
	**** Exception si : N�ant
	**************************************/
	char* CPXAfficher();
	



};

/**************************************
**** Surchage de l'op�rateur <<
***************************************
**** Entr�e: flux : ostream, CPXparam : CComplexe
**** N�cessite : N�ant
**** Sortie : N�ant
**** Entraine : Augmente la valeur de la partie r�elle de l'objet en cours de CPXparam.dCPXReelle et m�me chose pour la partie imaginaire
**** Exception si : N�ant
**************************************/
ostream &operator<<(ostream &flux, CComplexe &CPXparam);




#endif // !CComplexH








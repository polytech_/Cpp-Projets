#include "COperationMatComplexes.h"

CMatrice<double> COperationMatComplexes::OMCGetPartieReelle(CMatrice<CComplexe> matParam)
{
	unsigned int iuBoucleLigne = 0;
	unsigned int iuBoucleColonnes = 0;

	double ** pddMatrice = (double**)malloc(matParam.MATgetNbLignes()*sizeof(double*));
	for (iuBoucleLigne = 0; iuBoucleLigne < matParam.MATgetNbLignes(); iuBoucleLigne++) {
		pddMatrice[iuBoucleLigne] = (double*)malloc(matParam.MATgetNbColonnes() * sizeof(double));
		for (iuBoucleColonnes = 0; iuBoucleColonnes < matParam.MATgetNbColonnes(); iuBoucleColonnes++) {
			pddMatrice[iuBoucleLigne][iuBoucleColonnes] = matParam.getMatrice()[iuBoucleLigne][iuBoucleColonnes].CPXgetPartieReelle();
		}
	}

	CMatrice<double> MATRes(matParam.MATgetNbColonnes(), matParam.MATgetNbLignes(), pddMatrice);
	for (iuBoucleLigne = 0; iuBoucleLigne < matParam.MATgetNbColonnes(); iuBoucleLigne++) {
		free(pddMatrice[iuBoucleLigne]);
	}
	free(pddMatrice);

	return MATRes;
}

CMatrice<double> COperationMatComplexes::OMCGetPartieImaginaire(CMatrice<CComplexe> matParam)
{
	unsigned int iuBoucleLigne = 0;
	unsigned int iuBoucleColonnes = 0;

	double ** pddMatrice = (double**)malloc(matParam.MATgetNbLignes() * sizeof(double*));

	for (iuBoucleLigne = 0; iuBoucleLigne < matParam.MATgetNbLignes(); iuBoucleLigne++) {
		pddMatrice[iuBoucleLigne] = (double*)malloc(matParam.MATgetNbColonnes() * sizeof(double));
		for (iuBoucleColonnes = 0; iuBoucleColonnes < matParam.MATgetNbColonnes(); iuBoucleColonnes++) {
			pddMatrice[iuBoucleLigne][iuBoucleColonnes] = matParam.getMatrice()[iuBoucleLigne][iuBoucleColonnes].CPXgetPartieImaginaire();
		}
	}
	CMatrice<double> MATRes(matParam.MATgetNbColonnes(), matParam.MATgetNbLignes(), pddMatrice);

	for (iuBoucleLigne = 0; iuBoucleLigne < matParam.MATgetNbColonnes(); iuBoucleLigne++) {
		free(pddMatrice[iuBoucleLigne]);
	}
	free(pddMatrice);

	return  MATRes;
	
}

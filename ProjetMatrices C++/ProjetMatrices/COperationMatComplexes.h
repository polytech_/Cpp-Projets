#pragma once

#include "CMatrice.h"

class COperationMatComplexes
{
public:

	/**************************************
	**** M�thode statique permettant de retourner la partie r�elle d'une matrice complexe
	***************************************
	**** Entr�e: CMatrice<CComplexe> matParam
	**** N�cessite : N�ant
	**** Sortie : CMatrice<double>
	**** Entraine : Cr�ation et retour de la matrice compos�e uniquement des parties r�elles des objets CComplexes contenus dans matParam
	**** Exception si : N�ant
	**************************************/
	static CMatrice<double> OMCGetPartieReelle(CMatrice<CComplexe> matParam);

	/**************************************
	**** M�thode statique permettant de retourner la partie imaginaire d'une matrice complexe
	***************************************
	**** Entr�e: CMatrice<CComplexe> matParam
	**** N�cessite : N�ant
	**** Sortie : CMatrice<double>
	**** Entraine :  Cr�ation et retour de la matrice compos�e uniquement des parties imaginaires des objets CComplexes contenus dans matParam
	**** Exception si : N�ant
	**************************************/
	static CMatrice<double> OMCGetPartieImaginaire(CMatrice<CComplexe> matParam);
};


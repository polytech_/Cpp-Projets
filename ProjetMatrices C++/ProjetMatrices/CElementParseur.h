﻿#pragma once
#ifndef CElementParseurH
#define CElementParseurH 7
#include"CListe.h"



class CElementParseur
{

private:

	char* pcELPNomElement;


	CListe<char*>* pLISELPValeurElement;


public:

	

	/**************************************
	**** Constructeur par défaut
	***************************************
	**** Entrée: Néant
	**** Nécessite : Néant
	**** Sortie : Néant
	**** Entraine : Néant
	**** Exception si : Néant
	**************************************/
	CElementParseur();

	/**************************************
	**** Constructeur
	**************************************
	**** Entrée: -pcNom : chaine de caractere
	****         - pLISElem : pointeur vers une liste de chaine de caractere
	**** Nécessite : Néant
	**** Sortie : Néant
	**** Entraine : Néant
	**** Exception si : Néant
	**************************************/
	CElementParseur(char* pcNom, CListe<char*>* pLISElem);

	/**************************************
	**** Destructeur
	**************************************
	**** Entrée:
	**** Nécessite : Néant
	**** Sortie : Néant
	**** Entraine : Destruction de la liste
	**** Exception si : Néant
	**************************************/
	~CElementParseur();

	/**************************************
	**** Methode getter
	**************************************
	**** Entrée: Néant
	**** Nécessite : Néant
	**** Sortie : chaine de caractere
	**** Entraine : retourne la chaine
	**** Exception si : Néant
	**************************************/
	char* ELPGetNomElem();

	/**************************************
	**** Methode getter
	**************************************
	**** Entrée: Néant
	**** Nécessite : Néant
	**** Sortie : Une liste de chaines
	**** Entraine : retourne la liste en attribut
	**** Exception si : Néant
	**************************************/
	CListe<char*>* ELPGetValeurElem();

	/**************************************
	**** Methode setter
	**************************************
	**** Entrée: pcNom : chaine de caractere
	**** Nécessite : Néant
	**** Sortie : Néant
	**** Entraine : Modification de l'attribut nom
	**** Exception si : Néant
	**************************************/
	void ELPSetNomElem(char* pcNom);

	/**************************************
	**** Methode getter
	**************************************
	**** Entrée: pLISVal : liste de chaine de caractere
	**** Nécessite : Néant
	**** Sortie : Néant
	**** Entraine : Modification de l'attribut liste
	**** Exception si : Néant
	**************************************/
	void ELPSetValeurElem(CListe<char*>* pLISVal);


};




#endif // !CElementParseur 7



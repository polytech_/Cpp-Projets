#include "CParseur.h"
#define FICHIER_IMCOMPATIBLE 200
#define FICHIER_INTROUVABLE 202


#define TAILLE_CHAINE 256

#pragma warning(disable : 4996)




CParseur::CParseur()
{
	pLISPARElementsParseurs = new CListe<CElementParseur>();
}




CParseur::~CParseur()
{
	delete pLISPARElementsParseurs;
}




CListe<CElementParseur>* CParseur::PARLecture(const char* pcFichier) {

	char pcchaine[256];
	ifstream fichier(pcFichier, ios::in);
	char * pcElemGauche = nullptr;
	CListe<char*>* pLISElements = nullptr;
	char ** pElemDroite = nullptr;


	if (fichier)  // si l'ouverture a r�ussi
	{
		while (fichier.getline(pcchaine, 256)) {

			pLISElements = new CListe<char*>();

			pcElemGauche = PARgetElementGauche(pcchaine);

			if (bPARElemEstTableau(pcchaine)) {

				while (pcchaine[0] != ']') {
					fichier.getline(pcchaine, 256);
					if (pcchaine[0] != ']') {
						pElemDroite = new char*[1];
						*pElemDroite = PARCopyLigne(pcchaine);
						pLISElements->LISAJouter(pElemDroite);
					}

				}


			}
			else {
				pElemDroite = new char*[1];
				*pElemDroite = PARExtraireValeur(pcchaine);
				pLISElements->LISAJouter(pElemDroite);

			}

			pLISPARElementsParseurs->LISAJouter(new CElementParseur(pcElemGauche, pLISElements));







		}




		fichier.close();  // je referme le fichier
	}

	else {
		CExeption EXC1(FICHIER_INTROUVABLE);
		throw EXC1;
	}


	return pLISPARElementsParseurs;
}



CListe<CElementParseur>* CParseur::PARGetListeElement()
{
	return pLISPARElementsParseurs;
}





char* CParseur::PARgetElementGauche(char * pcLigne) {

	char* ElemGauche = (char*)malloc(TAILLE_CHAINE * sizeof(char));
	char carac = pcLigne[0];
	int icpt = 0;

	while (carac != '=') {
		carac = pcLigne[icpt];
		if (carac != '=') {
			ElemGauche[icpt] = carac;
			icpt++;
		}

	}
	ElemGauche[icpt] = '\0';


	return ElemGauche;

}






bool CParseur::bPARElemEstTableau(char * pcLigne)
{

	char carac = pcLigne[0];
	int icpt = 0;

	while (carac != '=') {
		carac = pcLigne[icpt];
		icpt++;
	}
	//icpt++;
	if (pcLigne[icpt] == '[') return true;
	else return false;


}




char *CParseur::PARExtraireValeur(char* pcligne) {

	char* chaine = (char*)malloc(TAILLE_CHAINE * sizeof(char));
	char carac = pcligne[0];
	unsigned int cpt = 0;
	unsigned int flag = 0;

	while (carac != '\0')
	{
		cpt++;
		carac = pcligne[cpt];

		if (flag > 0)
		{
			chaine[flag - 1] = pcligne[cpt];
			flag++;
		}

		if (carac == '=')
		{
			flag = 1;
		}

	}

	chaine[flag - 1] = '\0';

	return chaine;

}




char * CParseur::PARCopyLigne(char * pcLigne)
{
	char * pcLigneR = (char*)malloc(TAILLE_CHAINE * sizeof(char));
	char carac = pcLigne[0];
	unsigned int cpt = 0;

	while (carac != '\0') {
		carac = pcLigne[cpt];
		pcLigneR[cpt] = carac;
		cpt++;
	}
	pcLigneR[cpt] = '\0';
	return pcLigneR;
}





#include "CComplexe.h"

#define TAILLECHAINE 20


CComplexe::CComplexe()
{
	//useless
}

CComplexe::CComplexe(double dreelle, double dimaginaire)
{
	dCPXReelle = dreelle;
	dCPXImaginaire = dimaginaire;
}

CComplexe::CComplexe(char * pcNombre)
{
	char cSouschaineRe[] = "              ";
	char cSouschaineIm[] = "              ";

	unsigned int iCompteur = 0;
	unsigned int iCompteurIm = 0; 
	unsigned int uiBoucle;

	bool bReNegatif = false;

	if (strstr(pcNombre, "i")) {

		if (pcNombre[0] == '-') {
			for (uiBoucle = 1; uiBoucle < 14; uiBoucle++) {
				if (pcNombre[uiBoucle] == '+' || pcNombre[uiBoucle] == '-') {
					bReNegatif = true;
				}
			}
			if(bReNegatif) pcNombre[0] = ' ';




		}
		if (strstr(pcNombre, "+") || strstr(pcNombre, "-")) {
			while (pcNombre[iCompteur] != '+' && pcNombre[iCompteur] != '-') {
				cSouschaineRe[iCompteur] = pcNombre[iCompteur];
				iCompteur++;
			}

			if(bReNegatif) dCPXReelle = -atof(cSouschaineRe);
			else dCPXReelle = atof(cSouschaineRe);
			if(pcNombre[iCompteur] == '+') iCompteur++;

			while (pcNombre[iCompteur] != 'i') {
				cSouschaineIm[iCompteurIm] = pcNombre[iCompteur];
				iCompteurIm++;
				iCompteur++;
			}
			dCPXImaginaire = atof(cSouschaineIm);
			if (dCPXImaginaire == 0) dCPXImaginaire = 1;

		}
		else {
			dCPXReelle = 0;

			while (pcNombre[iCompteur] != 'i') {
				cSouschaineIm[iCompteur] = pcNombre[iCompteur];
				iCompteur++;
			}
			dCPXImaginaire = atof(cSouschaineIm);
			if (dCPXImaginaire == 0) dCPXImaginaire = 1;

		}

	}
	else {
		dCPXReelle = atof(pcNombre);
		dCPXImaginaire = 0;
	}


}



CComplexe::CComplexe(CComplexe & CPXparam)
{
	dCPXReelle = CPXparam.dCPXReelle;
	dCPXImaginaire = CPXparam.dCPXImaginaire;
}




double CComplexe::CPXgetPartieReelle()
{
	return dCPXReelle;
}



double CComplexe::CPXgetPartieImaginaire()
{
	return dCPXImaginaire;
}



void CComplexe::CPXsetPartieReelle(double dparam)
{
	dCPXReelle = dparam;
}



void CComplexe::CPXsetPartieImaginaire(double dparam)
{
	dCPXImaginaire = dparam;
}



/*
ostream &operator<<(ostream &flux, CComplexe &CPXparam)
{
	if (CPXparam.CPXgetPartieReelle() != 0)
	{
		flux << CPXparam.CPXgetPartieReelle();
	}

	if (CPXparam.CPXgetPartieImaginaire() < 0)
	{
		flux << "-";
		if(CPXparam.CPXgetPartieImaginaire() == -1)
		{
			flux<<"i" << " ";
		}
		else
		{
			flux << abs(CPXparam.CPXgetPartieImaginaire()) << "i ";
		}
	}

	if (CPXparam.CPXgetPartieImaginaire() > 0)
	{
		if (CPXparam.CPXgetPartieReelle() != 0)
		{
			flux << '+';
		}

		if (CPXparam.CPXgetPartieImaginaire() == 1)
		{
			flux << "i" << " ";
		}
		else
		{
			flux << CPXparam.CPXgetPartieImaginaire() << "i ";
		}
		
	}

	if (CPXparam.CPXgetPartieImaginaire() == 0 && CPXparam.CPXgetPartieReelle()==0)
	{
		flux << "0 ";
	}


	return flux;
}
*/

ostream &operator<<(ostream &flux, CComplexe &CPXparam)
{
	flux << CPXparam.CPXAfficher();
	return flux;
}

CComplexe CComplexe::operator+(CComplexe &CPXparam)
{
	CComplexe CPXSomme(dCPXReelle + CPXparam.CPXgetPartieReelle(), dCPXImaginaire + CPXparam.CPXgetPartieImaginaire());
	return CPXSomme;
}




CComplexe CComplexe::operator+(double dNombre)
{
	CComplexe CPXSomme(dCPXReelle + dNombre, dCPXImaginaire + dNombre);
	return CPXSomme;
}




CComplexe CComplexe::operator-(CComplexe & CPXparam)
{
	CComplexe CPXDifference(dCPXReelle - CPXparam.CPXgetPartieReelle(), dCPXImaginaire - CPXparam.CPXgetPartieImaginaire());
	return CPXDifference;
}



CComplexe CComplexe::operator-(double dNombre)
{
	CComplexe CPXDifference(dCPXReelle - dNombre, dCPXImaginaire - dNombre);
	return CPXDifference;
}



CComplexe CComplexe::operator*(CComplexe & CPXparam)
{
	CComplexe CPXProduit(dCPXReelle*CPXparam.CPXgetPartieReelle()-(dCPXImaginaire*CPXparam.CPXgetPartieImaginaire()), dCPXReelle*CPXparam.CPXgetPartieImaginaire()+ dCPXImaginaire* CPXparam.CPXgetPartieReelle());
	return CPXProduit;
}




CComplexe CComplexe::operator*(double dNombre)
{
	CComplexe CPXProduit(dCPXReelle*dNombre, dCPXImaginaire*dNombre);
	return CPXProduit;
}




CComplexe CComplexe::operator/(CComplexe & CPXparam)
{
	if (CPXparam.dCPXImaginaire == 0 && CPXparam.dCPXReelle==0)
	{
		CExeption EXC1(DENOMINATEUR_CPX_NUL);
		throw EXC1;
	}
	double denom = pow(CPXparam.CPXgetPartieReelle(),2) + pow(CPXparam.CPXgetPartieImaginaire(),2);
	CComplexe CPXDivision((dCPXReelle*CPXparam.CPXgetPartieReelle() - dCPXImaginaire*(-CPXparam.CPXgetPartieImaginaire()))/denom, (dCPXReelle*(-CPXparam.CPXgetPartieImaginaire()) + dCPXImaginaire * CPXparam.CPXgetPartieReelle())/denom);
	return CPXDivision;
}



CComplexe CComplexe::operator/(double dNombre)
{
	if (dNombre==0)
	{
		CExeption EXC1(DENOMINATEUR_CPX_NUL);
		throw EXC1;
	}
	else {
		CComplexe CPXDivision(dCPXReelle/dNombre, dCPXImaginaire/dNombre);
		return CPXDivision;
	}
	
}



void CComplexe::operator=(CComplexe &CPXparam)
{
	dCPXReelle = CPXparam.CPXgetPartieReelle();
	dCPXImaginaire = CPXparam.CPXgetPartieImaginaire();
}



void CComplexe::operator=(double dNombre)
{
	if (dNombre == 0) {
		dCPXReelle = 0;
		dCPXImaginaire = 0;
	}

	else {
		dCPXReelle = dNombre;
		dCPXImaginaire = 0;
	}

}


CComplexe CComplexe::operator+=(CComplexe &CPXparam)
{
	//CComplexe CPXResultat(dCPXReelle += CPXparam.CPXgetPartieReelle(), dCPXImaginaire += CPXgetPartieImaginaire());
	this->dCPXReelle += CPXparam.dCPXReelle;
	this->dCPXImaginaire += CPXparam.dCPXImaginaire;
	return *this;
}

char* CComplexe::CPXAfficher()
{

	static char cChaineRetour[TAILLECHAINE];
	cChaineRetour[0] = '\0';
	char cSouschaine[10];
	unsigned int iCompteur = 0;

	if (dCPXReelle != 0)
	{
		sprintf(cSouschaine, "%.2f", dCPXReelle);
		strcat(cChaineRetour,cSouschaine);
	}

	if (dCPXImaginaire < 0)
	{
		strcat(cChaineRetour, "-");
		if (dCPXImaginaire == -1)
		{
			strcat(cChaineRetour, "i ");
		}
		else
		{
			sprintf(cSouschaine, "%.2f", abs(dCPXImaginaire));
			strcat(cChaineRetour, cSouschaine);
			strcat(cChaineRetour, "i ");
		}
	}

	if (dCPXImaginaire > 0)
	{
		if (dCPXReelle != 0)
		{
			strcat(cChaineRetour, "+");
		}

		if (dCPXImaginaire == 1)
		{
			strcat(cChaineRetour, "i ");
		}
		else
		{
			sprintf(cSouschaine, "%.2f", abs(dCPXImaginaire));
			strcat(cChaineRetour, cSouschaine);
			strcat(cChaineRetour, "i ");
		}

	}

	if (dCPXImaginaire == 0 && dCPXReelle == 0)
	{
		strcat(cChaineRetour, "0");
	}

	while (cChaineRetour[iCompteur] != '\0') {
		iCompteur++;
	}
	while (iCompteur < TAILLECHAINE-1) {
		cChaineRetour[iCompteur] = ' ';
		iCompteur++;
	}
	cChaineRetour[iCompteur] = '\0';
	

	return cChaineRetour;
	
}

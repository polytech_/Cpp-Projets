#pragma once
#ifndef CExceptionH
#define CExceptionH 1



class CExeption
{
	private:
		unsigned int uiEXCNErreur;
	

	public:


		/**************************************
		**** Constructeur par d�faut
		***************************************
		**** Entr�e: N�ant
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : N�ant
		**** Exception si : N�ant
		**************************************/
		CExeption();



		/**************************************
		**** Constructeur de confort
		***************************************
		**** Entr�e: uiNErreur : int
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : Affecte l'entier uiNErreur pass� en param�tre � l'attribut uiEXCNErreur de l'objet en cours
		**** Exception si : N�ant
		**************************************/
		CExeption(unsigned int uiNErreur) { 
			uiEXCNErreur = uiNErreur; 
		}

		

		/**************************************
		**** Accesseur en lecture
		***************************************
		**** Entr�e: N�ant
		**** N�cessite : N�ant
		**** Sortie : le num�ro de l'erreur : unsigned int 
		**** Entraine : retourne le num�ro de l'erreur
		**** Exception si : N�ant
		**************************************/
		unsigned int EXCLireNErreur() { return uiEXCNErreur; }



		/**************************************
		**** Accesseur en �criture
		***************************************
		**** Entr�e: uiparam : unsigned int
		**** N�cessite : N�ant
		**** Sortie : unsigned int : le num�ro de l'erreur
		**** Entraine : Modifie le num�ro de l'erreur de l'objet en cours et le remplace par uiparam
		**** Exception si : N�ant
		**************************************/
		void EXCModifierErreur(unsigned int uiparam) { uiEXCNErreur = uiparam; }



		/**************************************
		**** Destructeur
		***************************************
		**** Entr�e: N�ant
		**** N�cessite : N�ant
		**** Sortie : N�ant
		**** Entraine : N�ant
		**** Exception si : N�ant
		**************************************/
		~CExeption();
};





#endif 

